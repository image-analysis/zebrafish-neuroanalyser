**NOTE**: <br>This location has been archived and the project has been relocated to:<br>
https://gitlab.com/exeter-zebrafish-research/zebrafish-neuroanalyser3d. 


# Zebrafish brain imaging 

This is a Python pipeline to perform similar processing to the Thunder project, 
without the need for Spark and associated libraries. 


## Running

### Command-line

From a terminal, use the correct python command (usually `python` or `python3`)
followed by `-m zebrafish_neuroanalyser`. 

The alignment GUI can be launched by passing in the `--aligngui` flag. 

#### Example

On a terminal where Python is run using `python3`, the commands to use are as follows. 

To run the main pipeline use: 

```
python3 -m zebrafish_neuroanalyser
```

To run the alignment gui use 

```
python3 -m zebrafish_neuroanalyser --aligngui
```

To see all command line options type 

```
python3 -m zebrafish_neuroanalyser -h
```

#### Linux (terminal)

Use the above commands from a properly configured Linux terminal. 

#### Windows (Anaconda Prompt or WinPython command prompt)

From an Anaconda Prompt or WinPython Command Prompt (both of which 
are simply pre-configured versions of the standard Windows command prompt, cmd.exe), 
use the above commands. 

### Windows file explorer

From windows file explorer, the main pipeline and the align-gui interface 
can be launched by double clicking on the `run_main.bat` and `run_align.bat` 
bat files, respectively. 

At present these are only configured for Anaconda, and require the location 
the anaconda install be correctly set at the top of the activate_anaconda.bat file. 

The default location is in the folder `Anaconda3` under the Users home directory, 
i.e. often this will be `C:\Users\<USERNAME>\Anaconda3` (where `<USERNAME>` is
replaced by the current user's username). 


