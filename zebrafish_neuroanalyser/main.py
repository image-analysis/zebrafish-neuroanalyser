#
# FILE        : process_neuro_data.py
# CREATED     : 05/10/15 18:25:06
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Python based neurological image stack processing pipeline
#

import skimage.feature
from scipy.ndimage import fourier_shift
import numpy as np
import scipy.ndimage as ndi
import argparse

# Command line directory completion
try:
    import readline
except Exception:
    print("readline module not imported...some functions may fail")
import glob

import os
import time
import traceback
import json
import io

# Identifying cluster - region relationships
from zebrafish_neuroanalyser.utility import (
    logger,
    default_error_function,
    default_get_filename_function,
    default_update_function,
    dummy_function,
    add_file_handler_to_global_logger,
)
from zebrafish_neuroanalyser.dataio import (
    load_and_resample,
    load_points_from_file,
    get_rois,
    save_full_profiles_to_csv,
    save_cluster_means_to_csv,
    save_corrected_labelled_region_means_to_csv,
    save_corrected_cluster_means_to_csv,
    save_labelled_region_means_to_csv,
    write_film_file,
)
from zebrafish_neuroanalyser.registration import (
    align_points,
)
from zebrafish_neuroanalyser.processing import (
    normalize_data,
    data_to_profiles,
)
from zebrafish_neuroanalyser.plotting import (
    plot_rois_on_max_proj,
    plot_label_region_means,
    plot_data,
    output_images_and_excel_sheets_for_time_projected_data,
    plot_lines,
    generate_labelled_regions_on_cluster_data_images,
    generate_cluster_images_projected,
    plot_cluster_coded_lines,
    output_cluster_stats,
)
from zebrafish_neuroanalyser.analysis import (
    get_stats_on_rois,
    run_clustering,
    run_peak_analysis_on_cluster_data,
    run_peak_analysis_on_region_data,
)
from zebrafish_neuroanalyser import identify_clusters

PWD = os.path.abspath(os.path.dirname(__file__))

# =========================================================
# The main event
# =========================================================


def main(
        filenames=[],
        update_function=default_update_function,
        error_message_function=default_error_function,
        get_filename_function=default_get_filename_function,
        output_path="",
        output_types=['png'],
        N_clusters=10,
        NLines=1000,
        align_frames=True,
        sample_factor=[3, 3],
        write_film=True,
        reference_rois_file=None,
        selected_rois_file=None,
        roifilearchive=None,
        # roifiledir=os.path.join(PWD, "labels", "individual_labels"),
        roifilebase="%06d.tif",
        TMIN=0,
        TMAX=0,
        n_rois=None,
        moviewriter=None,
        ):

    if selected_rois_file is None:
        selected_rois_file = os.path.join(PWD, "labels", "selected.txt")
    elif not os.path.isabs(selected_rois_file):
        # Assume a relative path was given
        selected_rois_file = os.path.join(PWD, "labels", selected_rois_file)

    if reference_rois_file is None:
        reference_rois_file = os.path.join(PWD, "labels", "rois.txt")

    if roifilearchive is None:
        roifilearchive = os.path.join(PWD, "labels", "labels.zip")

    if not output_path:
        raise Exception("Output path must be specified!")
    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    output_archive_filename = os.path.join(output_path, "detailed_data.zip")
    if os.path.isfile(output_archive_filename):
        os.remove(output_archive_filename)

    # This modifies the logger "in-place" - no need to pass in or return
    add_file_handler_to_global_logger(output_path)

    if filenames is None:
        filenames = get_filename_function()
    logger.info('Run started')
    logger.info('Input files:\n%s' % '\t'.join(filenames))
    update_function(message="Selected %d files" % len(filenames))
    # ------------------------------
    # Load and resample data
    # ------------------------------
    t0 = time.time()
    data, data0_shape = load_and_resample(
        filenames,
        sample_factor=sample_factor,
        update_function=update_function,
        align_on_load=align_frames,
        return_shape0=True,
        save_resampled=True,
        load_resampled=True,
        TMIN=TMIN,
        TMAX=TMAX,
    )
    update_function(message="Loaded into array: %s" % str(data.shape))
    logger.info('Loaded frames into array %s - took %f seconds' %
                (str(data.shape), time.time()-t0))

    # ------------------------------
    # For alignment of labels with data
    # ------------------------------
    # Get and load the data ROIs
    rois_file = os.path.join("%s_ROIS" % filenames[0], "rois.txt")
    pts = load_points_from_file(rois_file, shuffle=[2, 1, 0])

    # Load the reference ROIs
    ref_pts = load_points_from_file(reference_rois_file, shuffle=[2, 0, 1])

    update_function(
        message="Reference and data ROI points loaded, registering...")
    # Compute the transform
    affine, shift = align_points(ref_pts, pts)
    logger.debug("Registered Affine Transform and Shift:")
    logger.debug(str(affine))
    logger.debug(str(shift))

    # ------------------------------
    # Generate film
    # ------------------------------

    if write_film:
        if moviewriter is None:
            update_function(
                message="Unable to find video writer - skipping film...")
        else:
            write_film_file(data, output_path, moviewriter,
                            update_function=update_function)

    # ------------------------------
    # Basic "normalization" ie centering
    # ------------------------------
    update_function(message="Normalising data...")
    logger.info('Normalising data')
    datanorm = normalize_data(data)

    # ------------------------------
    # Extract labelled region data
    # ------------------------------

    allrois, roinames = get_rois(
        selected_rois_file,
        roifilearchive,  # roifiledir,
        roifilebase,
        affine,
        shift,
        data0_shape,
        sample_factor,
        n_rois,
        update_function=update_function,
        load_aligned_rois=True,
        save_aligned_rois=True,
        filenames=filenames,
    )
    # Show what we have
    plot_rois_on_max_proj(data.max(axis=0).max(axis=0), allrois, output_path)
    # For each selected ROI:
    stats = get_stats_on_rois(allrois, data)
    statsnorm = get_stats_on_rois(allrois, datanorm)

    # Plot label region means
    plot_label_region_means(
        stats, output_path, output_types, name="unnormalised")
    plot_label_region_means(statsnorm, output_path,
                            output_types, name="normalised")

    # ------------------------------
    # Plot first frame
    # ------------------------------
    update_function(message="Plotting 1st frame")
    for cbar_location in ["single", "multiple"]:
        plot_data(data,
                  filename="first_frame_unnormalised_cbar_%s" % cbar_location,
                  stat_function=lambda d: d[0],
                  imshow_kwargs=dict(cmap="jet"),
                  output_path=output_path,
                  output_types=output_types,
                  log_description="First frame of data",
                  show_colorbar=cbar_location,
                  )

    # ------------------------------
    # Min, Mean, Max...
    # ------------------------------
    update_function(message="Plotting minimum projection")
    # Min
    output_images_and_excel_sheets_for_time_projected_data(
        data.min(axis=0),
        "min",
        output_path,
        output_types,
        tics_min=5,
        tics_maj=20,
        tic_maj_font=8,
        tic_min_font=6,
        archive=output_archive_filename,
    )
    # mean
    update_function(message="Plotting mean projection")
    output_images_and_excel_sheets_for_time_projected_data(
        data.mean(axis=0),
        "mean",
        output_path,
        output_types,
        tics_min=5,
        tics_maj=20,
        tic_maj_font=8,
        tic_min_font=6,
        archive=output_archive_filename,
    )
    # max
    update_function(message="Plotting max projection")
    output_images_and_excel_sheets_for_time_projected_data(
        data.max(axis=0),
        "max",
        output_path,
        output_types,
        tics_min=5,
        tics_maj=20,
        tic_maj_font=8,
        tic_min_font=6,
        archive=output_archive_filename,
    )
    # ------------------------------
    # Plot mean over time
    plot_data(data,
              filename="mean_over_time_unnormalised",
              stat_function=np.mean,
              stat_kwargs=dict(axis=0),
              imshow_kwargs=dict(cmap="jet"),
              output_path=output_path,
              output_types=output_types,
              log_description="Mean of each pixel over time",
              contour_data=allrois,
              plot_individual=True,
              archive=output_archive_filename,
              show_colorbar="single",
              )
    # ------------------------------
    # Plot median over time
    plot_data(data,
              filename="median_over_time_unnormalised",
              stat_function=np.median,
              stat_kwargs=dict(axis=0),
              imshow_kwargs=dict(cmap="jet"),
              output_path=output_path,
              output_types=output_types,
              log_description="Median of each pixel over time",
              contour_data=allrois,
              plot_individual=True,
              archive=output_archive_filename,
              show_colorbar="single",
              )
    # Now try median, but after using minimum-filter baseline subtraction

    def median_of_baseline_corrected(dat):
        # input data should be shape T, X,Y,Z
        scale_bg = min(50, int(dat.shape[0]/2))
        dat2 = ndi.minimum_filter1d(dat, scale_bg, axis=0)
        return np.median(dat2, axis=0)

    plot_data(data,
              filename="median_over_time_unnormalised_corrected",
              stat_function=median_of_baseline_corrected,
              # stat_kwargs = dict(axis=0),
              imshow_kwargs=dict(cmap="jet"),
              output_path=output_path,
              output_types=output_types,
              log_description="Median of each pixel over time",
              contour_data=allrois,
              plot_individual=True,
              archive=output_archive_filename,
              show_colorbar="single",
              )

    # ------------------------------
    # Plot maximum intensity over time
    plot_data(data,
              filename="maximum_intensity_over_time_unnormalised",
              stat_function=np.max,
              stat_kwargs=dict(axis=0),
              imshow_kwargs=dict(cmap="jet"),
              output_path=output_path,
              output_types=output_types,
              log_description="Maximum intensity of each pixel over time",
              show_colorbar="single",
              )
    # ------------------------------
    # Plot Stdv (over time)
    plot_data(data,
              filename="standard_deviation_over_time_unnormalised",
              stat_function=np.std,
              stat_kwargs=dict(axis=0),
              imshow_kwargs=dict(cmap="jet"),
              output_path=output_path,
              output_types=output_types,
              log_description="Standard deviation of each pixel over time",
              show_colorbar="single",
              contour_data=allrois,
              plot_individual=True,
              archive=output_archive_filename,
              )
    # ------------------------------
    # Plot line profiles
    # ------------------------------
    update_function(message="Plotting line profiles...")
    plot_lines(
        data_to_profiles(data),
        filename="raw_profiles",
        update_function=update_function,
        output_path=output_path,
        output_types=output_types,
        log_description=" ".join((
            "Raw line profiles ",
            "(intensities of each pixel over time)")),
    )

    linesnormed = data_to_profiles(datanorm)
    update_function(message="DONE [new shape: %s]" % str(linesnormed.shape))
    plot_lines(
        linesnormed,
        filename="normed_profiles",
        update_function=update_function,
        output_path=output_path,
        output_types=output_types,
        log_description="".join((
            "Centered line profiles ",
            "(intensities of each pixel over time)")),
    )
    # ------------------------------
    # Perform kmeans clustering on the lines
    # ------------------------------
    linelabels = run_clustering(
        linesnormed,
        n_clusters=N_clusters,
        update_function=update_function)
    # ------------------------------
    # Reshape into volume / image ancbar_location
    # ------------------------------
    update_function(message="Finished mini-batch k-means, reshaping labels")
    labels = linelabels.reshape(datanorm.shape[1:])

    # ------------------------------
    # Plot labels on top of cluster image...
    # ------------------------------
    generate_labelled_regions_on_cluster_data_images(
        labels,
        N_clusters,
        allrois,
        output_path,
        output_archive_filename,
    )

    # -------------------
    # raw region & cluster data output added 2016/10/14 11:37:17 (BST)
    # would have been good to have this before but hey ho!

    # First the cluster index data
    np.savez_compressed(
        os.path.join(output_path, "cluster_index_array.npz"),
        clusters=labels)
    # Now also the labelled region data
    np.savez_compressed(
        os.path.join(output_path, "labelled_region_index_arrays.npz"),
        **{str(k): v for k, v in allrois.items()})

    identify_clusters.analyze_label_cluster_relationships_and_save(
        allrois, labels,
        filename=os.path.join(
            output_path, "cluster_labelled_region_relationships.csv")
        )

    # -------------------
    # Cluster images
    update_function(message="Plotting cluster images")
    generate_cluster_images_projected(
        labels,
        N_clusters,
        output_path,
        output_types,
    )

    # ------------------------------
    # Plot with cluster-index colour coded lines
    # ------------------------------
    # NOTE: This one is an anomaly - no point in creating a funtion yet
    plot_cluster_coded_lines(
        linesnormed,
        NLines,
        N_clusters,
        linelabels,
        output_path,
        output_types,
        update_function,
    )
    # ------------------------------
    # Calculate means and stdvs of clusters
    # ------------------------------

    cluster_means, cluster_stdv = output_cluster_stats(
        linesnormed,
        N_clusters, linelabels,
        output_path,
        output_types,
        update_function,
    )

    # ------------------------------
    # Segmentation / analysis of peaks
    # ------------------------------

    corrected_cluster_means = run_peak_analysis_on_cluster_data(
        cluster_means,
        output_path,
        output_types,
        update_function=update_function,
    )

    # ------------------------------
    # Same analysis on labelled region data

    run_peak_analysis_on_region_data(
        stats,
        output_path,
        output_types,
        update_function=update_function,
        name="unnormalised",
    )
    run_peak_analysis_on_region_data(
        statsnorm,
        output_path,
        output_types,
        update_function=update_function,
        name="normalised",
    )

    # ------------------------------
    # Save output as CSV files, using numpy save
    # ------------------------------

    update_function(message="Saving full profiles to CSV")
    save_full_profiles_to_csv(linesnormed, linelabels,
                              output_path, output_archive_filename)
    # Save cluster means & standard deviations
    update_function(message="Saving cluster means to CSV")
    save_cluster_means_to_csv(
        cluster_means, cluster_stdv, N_clusters, output_path)
    # Save labelled region means & standard deviations
    update_function(message="Saving labelled region means to CSV")
    save_labelled_region_means_to_csv(
        stats, roinames, output_path, name="unnormalised")
    save_labelled_region_means_to_csv(
        statsnorm, roinames, output_path, name="normalised")
    # Save the corrected cluster means
    update_function(message="Saving corrected cluster means to CSV")
    save_corrected_cluster_means_to_csv(corrected_cluster_means, output_path)
    # Corrected labelled region means
    update_function(
        message="Saving corrected region means to CSV (unnormalised)")
    save_corrected_labelled_region_means_to_csv(
        stats, corrected_cluster_means,
        roinames, output_path, name="unnormalised")
    update_function(
        message="Saving corrected region means to CSV (normalised)")
    save_corrected_labelled_region_means_to_csv(
        statsnorm, corrected_cluster_means,
        roinames, output_path, name="normalised")

    update_function(
        message="Main process_neuro_data function completed successfully!")
    logger.info("Main process_neuro_data function completed successfully!")
    return True
