#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Get stats etc
#

from zebrafish_neuroanalyser.utility import (
    default_update_function,
    logger,
)
from zebrafish_neuroanalyser.plotting import (
    savefig,
)
import numpy as np
import time
from sklearn.cluster import MiniBatchKMeans
import matplotlib.pyplot as plt
import scipy.ndimage as ndi
import os
import csv


def get_stats_on_rois(allrois, data):
    """
    Get some stats for each roi (mean, median, stdv)
    """
    stats = {}
    for roiname, roi in allrois.items():
        # Extract the corresponding data
        # datanow = (f[lbl2>0] for f in data)
        # Do this below; the generator was causing issues.
        # Perform whatever stats
        if not roi.any():
            stats[roiname] = {
                'mean': [0 for f in data],
                'median': [0 for f in data],
                'stdv': [0 for f in data],
            }
        else:
            stats[roiname] = {
                'mean': [f[roi].mean() for f in data],
                'median': [np.median(f[roi]) for f in data],
                'stdv': [f[roi].std() for f in data],
            }
    return stats


def run_clustering(lines, n_clusters, update_function=default_update_function):
    update_function(
        message="Performing mini-batch k-means clustering...(%d clusters)"
                % n_clusters)
    t0 = time.time()
    est = MiniBatchKMeans(n_clusters=n_clusters)

    est.fit(lines)
    linelabels = est.labels_
    logger.info("Performed Mini-batch Kmeans clustering in %f seconds" %
                (time.time()-t0))
    return linelabels

# ------------------------------
# Peak analysis
# ------------------------------


def run_peak_analysis_on_cluster_data(
        cluster_means,
        output_path,
        output_types,
        update_function=default_update_function,
        scale_filt=1.5,
        ):

    update_function(message="Analysing cluster profiles...")
    # First do segmentation on each profile individually
    N_clusters = cluster_means.shape[0]
    scale_bg = min(50, int(cluster_means.shape[1]/2))
    cmap = plt.cm.jet
    # ------------------------------
    # First estimate and remove baseline
    # ------------------------------
    methods = {
        "minimum": lambda f: ndi.minimum_filter(f, [scale_bg]),
        # "median"  : lambda f : ndi.median_filter(f, scale_bg),
    }
    corrected_means = {
        key: np.empty(cluster_means.shape)
        for key in methods
    }

    for im, m in enumerate(cluster_means):
        # fine scale smooth
        filt = ndi.gaussian_filter(m, scale_filt)
        for method, func in methods.items():
            corrected_means[method][im] = filt-func(filt)

    # ------------------------------
    # Now for each version, do the analysis
    # ------------------------------

    for method, corr_means in corrected_means.items():
        ids = []
        widths = []
        positions = []
        peakvalues = []
        aucs = []
        peakseperations = []
        fig = plt.figure()
        for im, filt in enumerate(corr_means):
            widths.append([])
            ids.append(im)
            t = range(len(filt))
            col = cmap(float(im)/(N_clusters-1))
            positions.append([])
            peakvalues.append([])
            aucs.append([])

            std = filt.std()
            bwpeaks = filt > (2*std)
            lbl, N = ndi.label(bwpeaks)
            for l in range(1, N+1):
                bw = lbl == l
                width = bw.sum()
                if(width < 2):
                    continue
                # For each segment, get the maximum (value and position)
                # and the width
                vmax = filt[bw].max()
                pos = np.argmax(bw*filt)

                widths[im].append(width)
                positions[im].append(pos)
                peakvalues[im].append(vmax)
                aucs[im].append(filt[bw].sum())

            plt.plot(t, filt, color=col)
            diffs = np.insert(np.diff(bwpeaks.astype(int)), 0, 0)
            diffs = np.append(diffs, 0)
            starts = (diffs == 1).nonzero()[0]
            ends = (diffs == -1).nonzero()[0]
            ax = plt.gca()
            for s, e in zip(starts, ends):
                ax.axvspan(s, e, alpha=0.4, color='yellow')

            for p, w in zip(positions[im], widths[im]):
                plt.plot(t[p], filt[p], 'ko')
                plt.text(t[p], filt[p], "t=%d\ny=%g\nwidth=%d" %
                         (t[p], filt[p], w))
            peakseperations.append(np.diff(positions[im]).tolist())
        savefig(fig, "cluster_analysis_%s" % method,
                output_path, output_types)

        # Save the positions, widths, and peakvalues
        with open(os.path.join(
                output_path,
                "cluster_mean_analysis_%s.csv" % method), "w") as csvfile:
            wrt = csv.writer(csvfile)
            for i in ids:
                wrt.writerow(["Cluster ID = %d" % i, ])
                wrt.writerow(["Peak positions (frame no.)", ] + positions[i])
                wrt.writerow(
                    ["Peak value (intensity above BG)", ] + peakvalues[i])
                wrt.writerow(["Peak width (pixels)", ] + widths[i])
                wrt.writerow(["Peak seperations (frames)", ] +
                             peakseperations[i])
                wrt.writerow(
                    ["Area under curve (summed intensities)", ] + aucs[i])
        # Reformatted version
        with open(os.path.join(
                output_path,
                "cluster_mean_analysis_reformatted_%s.csv" % method),
                "w") as csvfile:
            wrt = csv.writer(csvfile)
            for i in ids:
                wrt.writerow(["Cluster ID = %d" % i,
                              "Peak positions (frame no.)", ] + positions[i])
            for i in ids:
                wrt.writerow([
                    "Cluster ID = %d" % i,
                    "Peak value (intensity above BG)", ] + peakvalues[i])
            for i in ids:
                wrt.writerow(["Cluster ID = %d" % i,
                              "Peak width (pixels)", ] + widths[i])
            for i in ids:
                wrt.writerow([
                    "Cluster ID = %d" % i,
                    "Peak seperations (frames)", ] + peakseperations[i])
            for i in ids:
                wrt.writerow([
                    "Cluster ID = %d" % i,
                    "Area under curve (summed intensities)", ] + aucs[i])

        with open(os.path.join(
                output_path,
                "cluster_mean_analysis_%s_summary.csv" % method),
                "w") as csvfile:
            wrt = csv.writer(csvfile)
            wrt.writerow(["Peak value", ])
            wrt.writerow(["Cluster Id", "Mean", "Median",
                          "Std dev", "N", "Std err"])
            for i in ids:
                wrt.writerow([
                    i,
                    np.mean(peakvalues[i]),
                    np.median(peakvalues[i]),
                    np.std(peakvalues[i]),
                    len(peakvalues[i]),
                    np.std(peakvalues[i])/np.sqrt(len(peakvalues[i])),
                ])
            wrt.writerow(["Area under curve", ])
            wrt.writerow(["Id", "Mean", "Median", "Std dev", "N", "Std err"])
            for i in ids:
                wrt.writerow([
                    i,
                    np.mean(aucs[i]),
                    np.median(aucs[i]),
                    np.std(aucs[i]),
                    len(aucs[i]),
                    np.std(aucs[i])/np.sqrt(len(aucs[i])),
                ])
            wrt.writerow(["Peak width", ])
            wrt.writerow(["Id", "Mean", "Median", "Std dev", "N", "Std err"])
            for i in ids:
                wrt.writerow([
                    i,
                    np.mean(widths[i]),
                    np.median(widths[i]),
                    np.std(widths[i]),
                    len(widths[i]),
                    np.std(widths[i])/np.sqrt(len(widths[i])),
                ])
            wrt.writerow(["Peak separation", ])
            wrt.writerow(["Id", "Mean", "Median", "Std dev", "N", "Std err"])
            for i in ids:
                wrt.writerow([
                    i,
                    np.mean(peakseperations[i]),
                    np.median(peakseperations[i]),
                    np.std(peakseperations[i]),
                    len(peakseperations[i]),
                    np.std(peakseperations[i]) /
                    np.sqrt(len(peakseperations[i])),
                ])
    return corrected_means


def run_peak_analysis_on_region_data(
        stats,
        output_path,
        output_types,
        scale_filt=1.5,
        update_function=default_update_function,
        name="",
        ):
    update_function(
        message="Analysing region profiles - peak analysis %s" % name)
    if name:
        name = "_%s" % name
    key = next(iter(stats.keys()))
    scale_bg = min(50, len(stats[key]["mean"])//2)
    methods = {
        "minimum": lambda f: ndi.minimum_filter(f, [scale_bg]),
        # "median"  : lambda f : ndi.median_filter(f, scale_bg),
    }

    for key in stats:
        filt = ndi.gaussian_filter(stats[key]['mean'], scale_filt)
        filtmed = ndi.gaussian_filter(stats[key]['median'], scale_filt)
        filtstd = ndi.gaussian_filter(stats[key]['stdv'], scale_filt)
        stats[key]['corrected means'] = {}
        stats[key]['corrected medians'] = {}
        stats[key]['corrected stdv'] = {}
        for method, func in methods.items():
            stats[key]['corrected means'][method] = filt-func(filt)
            stats[key]['corrected medians'][method] = filtmed-func(filtmed)
            stats[key]['corrected stdv'][method] = filtstd-func(filtstd)

    run_peak_analysis_on_corrected_stats_dict(
        stats,
        output_path,
        output_types,
        update_function=default_update_function,
        name="")


def run_peak_analysis_on_corrected_stats_dict(
        *args_run_peak_analysis_on_corrected_stats_dict_single_stat,
        statnames=["corrected means", "corrected medians"],
        **kwargs_run_peak_analysis_on_corrected_stats_dict_single_stat,
        ):
    for statname in statnames:
        run_peak_analysis_on_corrected_stats_dict_single_stat(
            *args_run_peak_analysis_on_corrected_stats_dict_single_stat,
            statname=statname,
            **kwargs_run_peak_analysis_on_corrected_stats_dict_single_stat,
        )


def run_peak_analysis_on_corrected_stats_dict_single_stat(
        stats,
        output_path,
        output_types,
        update_function=default_update_function,
        name=None,
        cmap=plt.cm.jet,
        statname="corrected means",
        ):

    statstr = statname.replace(" ", "_")
    if not name:
        raise ValueError("The name keyword argument must be set")
    methods = list(next(iter(next(iter(stats.values())).values())).keys())

    for method in methods:
        ids = []
        widths = []
        positions = []
        peakvalues = []
        aucs = []
        peakseperations = []
        fig = plt.figure()
        for im, key in enumerate(stats):
            filt = stats[key][statname][method]
            widths.append([])
            ids.append(key)
            t = range(len(filt))
            col = cmap(float(key)/(len(stats)-1+1E-16))
            positions.append([])
            peakvalues.append([])
            aucs.append([])

            std = filt.std()
            bwpeaks = filt > (2*std)
            lbl, N = ndi.label(bwpeaks)
            for l in range(1, N+1):
                bw = lbl == l
                width = bw.sum()
                if(width < 2):
                    continue
                # For each segment, get the maximum (value and position)
                # and the width
                vmax = filt[bw].max()
                pos = np.argmax(bw*filt)

                widths[im].append(width)
                positions[im].append(pos)
                peakvalues[im].append(vmax)
                aucs[im].append(filt[bw].sum())

            plt.plot(t, filt, color=col)
            diffs = np.insert(np.diff(bwpeaks.astype(int)), 0, 0)
            diffs = np.append(diffs, 0)
            starts = (diffs == 1).nonzero()[0]
            ends = (diffs == -1).nonzero()[0]
            ax = plt.gca()
            for s, e in zip(starts, ends):
                ax.axvspan(s, e, alpha=0.4, color='yellow')

            for p, w in zip(positions[im], widths[im]):
                plt.plot(t[p], filt[p], 'ko')
                plt.text(t[p], filt[p], "t=%d\ny=%g\nwidth=%d" %
                         (t[p], filt[p], w))
            peakseperations.append(np.diff(positions[im]).tolist())
        savefig(fig, "labelled_analysis_%s_%s%s" % (statstr, method, name),
                output_path, output_types)

        # Save the positions, widths, and peakvalues
        with open(os.path.join(
                output_path,
                "labelled_%s_analysis_%s%s.csv" % (statstr, method, name)),
                "w") as csvfile:
            wrt = csv.writer(csvfile)
            for i, key in enumerate(ids):
                wrt.writerow(["ROI = %d" % key, ])
                wrt.writerow(["Peak positions (frame no.)", ] + positions[i])
                wrt.writerow(
                    ["Peak value (intensity above BG)", ] + peakvalues[i])
                wrt.writerow(["Peak width (pixels)", ] + widths[i])
                wrt.writerow(["Peak seperations (frames)", ] +
                             peakseperations[i])
                wrt.writerow(
                    ["Area under curve (summed intensities)", ] + aucs[i])
        # Reformatted version
        with open(os.path.join(
                output_path,
                "labelled_%s_analysis_reformatted_%s%s.csv"
                % (statstr, method, name)), "w") as csvfile:
            wrt = csv.writer(csvfile)
            for i, key in enumerate(ids):
                wrt.writerow(["ROI = %d" % key,
                              "Peak positions (frame no.)", ] + positions[i])
            for i, key in enumerate(ids):
                wrt.writerow([
                    "ROI = %d" % key,
                    "Peak value (intensity above BG)", ] + peakvalues[i])
            for i, key in enumerate(ids):
                wrt.writerow(["ROI = %d" % key,
                              "Peak width (pixels)", ] + widths[i])
            for i, key in enumerate(ids):
                wrt.writerow([
                    "ROI = %d" % key,
                    "Peak seperations (frames)", ] + peakseperations[i])
            for i, key in enumerate(ids):
                wrt.writerow([
                    "ROI = %d" % key,
                    "Area under curve (summed intensities)", ] + aucs[i])
        # Also save those values
        with open(os.path.join(
                    output_path,
                    "labelled_%s_analysis_%s%s_summary.csv" % (
                        statstr, method, name)),
                  "w") as csvfile:
            wrt = csv.writer(csvfile)
            wrt.writerow(["Peak value", ])
            wrt.writerow(["Id", "Mean", "Median", "Std dev", "N", "Std err"])
            for i, key in enumerate(ids):
                wrt.writerow([
                    key,
                    np.mean(peakvalues[i]),
                    np.median(peakvalues[i]),
                    np.std(peakvalues[i]),
                    len(peakvalues[i]),
                    np.std(peakvalues[i])/np.sqrt(len(peakvalues[i])),
                ])
            wrt.writerow(["Area under curve", ])
            wrt.writerow(["Id", "Mean", "Median", "Std dev", "N", "Std err"])
            for i, key in enumerate(ids):
                wrt.writerow([
                    key,
                    np.mean(aucs[i]),
                    np.median(aucs[i]),
                    np.std(aucs[i]),
                    len(aucs[i]),
                    np.std(aucs[i])/np.sqrt(len(aucs[i])),
                ])
            wrt.writerow(["Peak width", ])
            wrt.writerow(["Id", "Mean", "Median", "Std dev", "N", "Std err"])
            for i, key in enumerate(ids):
                wrt.writerow([
                    key,
                    np.mean(widths[i]),
                    np.median(widths[i]),
                    np.std(widths[i]),
                    len(widths[i]),
                    np.std(widths[i])/np.sqrt(len(widths[i])),
                ])
            wrt.writerow(["Peak separation", ])
            wrt.writerow(["Id", "Mean", "Median", "Std dev", "N", "Std err"])
            for i, key in enumerate(ids):
                wrt.writerow([
                    key,
                    np.mean(peakseperations[i]),
                    np.median(peakseperations[i]),
                    np.std(peakseperations[i]),
                    len(peakseperations[i]),
                    np.std(peakseperations[i]) /
                    np.sqrt(len(peakseperations[i])),
                ])
