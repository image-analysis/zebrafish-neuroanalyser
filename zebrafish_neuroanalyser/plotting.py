#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Plotting and visualisation functions
#
import numpy as np
import matplotlib.pyplot as plt
from zebrafish_neuroanalyser.utility import (
    default_update_function,
    logger,
)
from zebrafish_neuroanalyser.dataio import (
    skioimsave,
    put_in_archive,
)
import os
from mpl_toolkits.axes_grid1 import AxesGrid
try:
    from mpl_toolkits.axes_grid1.anchored_artists import AnchoredText
except Exception:
    from matplotlib.offsetbox import AnchoredText
from openpyxl import Workbook


def plot_rois_on_max_proj(im_max, allrois, output_path):
    figlabels = plt.figure(figsize=(16, 12))
    axlabels = figlabels.add_subplot(111)
    axlabels.imshow(im_max, cmap='gray')
    for i, (roiname, roi) in enumerate(allrois.items()):
        immask = roi.max(axis=0)
        CS = axlabels.contour(immask, levels=[0.5, ], colors=[
                              plt.cm.jet((i % 11)/10.0), ])
        plt.clabel(CS, CS.levels, inline=1, fmt={0.5: str(i)}, fontsize=10)

    figlabels.savefig(os.path.join(output_path, "labelled_regions.png"))
    figlabels.savefig(os.path.join(output_path, "labelled_regions.svg"))

    figlabels = plt.figure(figsize=(16, 12))
    axlabels = figlabels.add_subplot(111)
    axlabels.imshow(im_max, cmap='inferno')
    for i, (roiname, roi) in enumerate(allrois.items()):
        immask = roi.max(axis=0)
        CS = axlabels.contour(immask, levels=[0.5, ], colors=[
                              plt.cm.jet((i % 11)/10.0), ])
        plt.clabel(CS, CS.levels, inline=1, fmt={0.5: str(i)}, fontsize=10)

    figlabels.savefig(os.path.join(output_path, "labelled_regions_cmap2.png"))
    figlabels.savefig(os.path.join(output_path, "labelled_regions_cmap2.svg"))
    plt.close()


def plot_label_region_means(stats, output_path, output_types, name=""):
    fig = plt.figure()
    # cmap = plt.cm.jet
    for key, stat in stats.items():
        m = np.array(stat['mean'])
        s = np.array(stat['stdv'])
        t = range(len(m))
        # col = cmap(float(l)/(N_clusters-1))
        lines = plt.plot(m,)  # color=col )
        col = lines[0].get_color()
        plt.fill_between(t, m-s, m+s, facecolor=col, alpha=0.5)
    savefig(fig, "labelled_region_means_%s" % name, output_path, output_types)
    plt.close(fig)


def output_images_and_excel_sheets_for_time_projected_data(
        ims,
        name,
        output_path,
        output_types,
        tics_min=5,
        tics_maj=20,
        tic_maj_font=8,
        tic_min_font=6,
        archive="",
        ):

    wb = Workbook()
    for i, im in enumerate(ims):
        # Generate image
        fig = plt.figure()
        ax = fig.add_subplot(111)
        plt.imshow(im, interpolation='nearest', origin="upper")
        plt.colorbar()
        ax.set_xticks(np.arange(0, im.shape[1], tics_maj))
        ax.set_xticks(np.arange(0, im.shape[1], tics_min), minor=True)
        ax.set_yticks(np.arange(0, im.shape[0], tics_maj))
        ax.set_yticks(np.arange(0, im.shape[0], tics_min), minor=True)
        ax.grid(b=True, which='both')
        ax.tick_params(axis='both', which='major', labelsize=tic_maj_font)
        ax.tick_params(axis='both', which='minor', labelsize=tic_min_font)
        savedfiles = savefig(fig, "%s_z_%d" %
                             (name, i), output_path, output_types)
        if archive:
            put_in_archive(savedfiles, archive)
        plt.close(fig)

        # Generate excel sheets
        ws = wb.create_sheet()
        ws.title = "z=%d" % i
        for row in im:
            ws.append(row.tolist())
    wb.save(os.path.join(output_path, "%s_data.xlsx" % name))
    # Generate raw images
    for z, im in enumerate(ims):
        fname = os.path.join(output_path, "%s_data_z_%d.png" % (name, z))
        skioimsave(fname, im.astype('uint16'))
        if archive:
            put_in_archive(fname, archive)


def generate_labelled_regions_on_cluster_data_images(
        labels,
        N_clusters,
        allrois,
        output_path,
        output_archive_filename,
        ):
    """
    Overlay the labelled regions on the clustered data images
    """
    clust_label_figs = []
    clust_label_axes = []
    for i, zclust in enumerate(labels):
        fig = plt.figure()
        axnow = fig.add_subplot(111)
        axnow.imshow(zclust,
                     vmin=-0.5,
                     vmax=N_clusters-0.5,
                     cmap=plt.get_cmap('winter', N_clusters),
                     )
        clust_label_figs.append(fig)
        clust_label_axes.append(axnow)

    for i, (roiname, lbl) in enumerate(allrois.items()):
        logger.debug("ADDING ROI: %s" % str(roiname))
        for z, ax in enumerate(clust_label_axes):
            CS = ax.contour(lbl[z], levels=[0.5, ],
                            colors=[plt.cm.spring(i/(len(allrois)-1+1E-16)), ]
                            )
            plt.clabel(CS, CS.levels, inline=1, fmt={
                       0.5: str(roiname)}, fontsize=10)

    for z, fig in enumerate(clust_label_figs):
        fname = os.path.join(
            output_path, "labelled_regions_on_cluster_data_z_%d.png" % z)
        fig.savefig(fname)
        put_in_archive(fname, output_archive_filename)
        plt.close()


def generate_cluster_images_projected(
        labels,
        N_clusters,
        output_path,
        output_types,
        ):
    """
    Show cluster images projected along x,y,z
    """

    plot_data(labels,
              filename="cluster_image_z",
              stat_function=None,
              imshow_kwargs=dict(
                  vmin=-0.5,
                  vmax=N_clusters-0.5,
                  cmap=plt.get_cmap('jet', N_clusters),
              ),
              output_path=output_path,
              output_types=output_types,
              log_description="".join((
                "Pixel cluster identity image",
                "- which cluster each pixel belongs to - along z")),
              show_colorbar="single",
              cbar_ticks=[int(i) for i in range(N_clusters)],
              )

    lx = labels.swapaxes(0, 1)
    inds = np.linspace(0, lx.shape[0], 11).astype(int)
    inds = inds[1:-1]
    lx = lx[inds]
    plot_data(lx,
              filename="cluster_image_x",
              stat_function=None,
              imshow_kwargs=dict(
                  vmin=-0.5,
                  vmax=N_clusters-0.5,
                  cmap=plt.get_cmap('jet', N_clusters),
              ),
              output_path=output_path,
              output_types=output_types,
              log_description="".join((
                "Pixel cluster identity image",
                "- which cluster each pixel belongs to - along x")),
              show_colorbar="single",
              cbar_ticks=[int(i) for i in range(N_clusters)],
              )

    ly = labels.swapaxes(0, 2)
    inds = np.linspace(0, ly.shape[0], 11).astype(int)
    inds = inds[1:-1]
    ly = ly[inds]
    plot_data(ly,
              filename="cluster_image_y",
              stat_function=None,
              imshow_kwargs=dict(
                  vmin=-0.5,
                  vmax=N_clusters-0.5,
                  cmap=plt.get_cmap('jet', N_clusters),
              ),
              output_path=output_path,
              output_types=output_types,
              log_description="".join((
                "Pixel cluster identity image",
                "- which cluster each pixel belongs to - along y")),
              show_colorbar="single",
              cbar_ticks=[int(i) for i in range(N_clusters)],
              )


def plot_cluster_coded_lines(
        linesnormed,
        NLines,
        N_clusters,
        linelabels,
        output_path,
        output_types,
        update_function,
        ):
    """
    Plot cluster-index colour coded lines
    """
    update_function(
        message="Plotting cluster-coded lines...[%d clusters]" % N_clusters)
    fig = plt.figure()
    cmap = plt.cm.jet
    Nlines_per_cluster = int(NLines/N_clusters)
    update_function(message="Plotting %d lines per cluster" %
                    Nlines_per_cluster)
    for l in range(N_clusters):
        col = cmap(float(l)/(N_clusters-1))
        if 0 in linesnormed[linelabels == l][:Nlines_per_cluster, :].T.shape:
            update_function(message="SKIPPING CLUSTER ID %d (EMPTY LINES)" % l)
            continue
        plt.plot(linesnormed[linelabels == l]
                 [:Nlines_per_cluster, :].T, color=col)
    savefig(fig, "Clustered_Profiles_normed", output_path, output_types)
    plt.close(fig)
    logger.info(
        "OUTPUT: %s  -  %s" % (
            "Clustered_Profiles_normed",
            "".join(("Replot of centered profiles, "
                     "but now colour coded with cluster identity")),
        )
    )

    update_function(message="Finished plotting cluster-coded lines")


def output_cluster_stats(
        linesnormed,
        N_clusters,
        linelabels,
        output_path,
        output_types,
        update_function,
        ):
    """
    Calculate and output cluster means and stdvs
    """
    update_function(message="Calculating stats...(line shape= %s)" %
                    str(linesnormed.shape))
    cluster_means = np.empty((N_clusters, linesnormed.shape[1]))
    cluster_stdv = np.empty((N_clusters, linesnormed.shape[1]))
    for l in range(N_clusters):
        cluster_means[l] = linesnormed[linelabels == l, :].mean(axis=0)
        cluster_stdv[l] = linesnormed[linelabels == l, :].std(axis=0)

    fig = plt.figure()
    cmap = plt.cm.jet
    for l in range(N_clusters):
        m = cluster_means[l]
        s = cluster_stdv[l]
        t = range(len(m))
        col = cmap(float(l)/(N_clusters-1))
        plt.plot(m, color=col)
        plt.fill_between(t, m-s, m+s, facecolor=col, alpha=0.5)
    savefig(fig, "cluster_means", output_path, output_types)
    plt.close(fig)
    logger.info(
        "OUTPUT: %s  -  %s" % (
            "cluster_means",
            "Profile means with standard deviations of each cluster")
    )

    return cluster_means, cluster_stdv


def savefig(fig, name, output_path, output_types):
    """
    Utility sub-function for saving a figure
    """
    fnames = [os.path.join(output_path, "%s.%s" % (name, ext))
              for ext in output_types]
    for fname in fnames:
        fig.savefig(fname)
    return fnames


def plot_data(
        data,
        filename=None,
        stat_function=np.max,
        stat_kwargs={},
        close=True,
        imshow_kwargs={},
        output_path="",
        output_types=[],
        log_description="",
        show_colorbar="",
        cbar_ticks=[],
        contour_data=None,
        plot_individual=False,
        archive="",
        ):
    fig = plt.figure()

    if show_colorbar == "single":
        axesgrid_extra_kwargs = {
            'cbar_location': 'right',
            'cbar_mode': 'single',
        }
    elif show_colorbar == "multiple":
        axesgrid_extra_kwargs = {
            'cbar_location': 'right',
            'cbar_mode': 'each',
        }
    elif not show_colorbar:
        axesgrid_extra_kwargs = {}
    if cbar_ticks:
        cbar_kwargs = {'ticks': cbar_ticks}
    else:
        cbar_kwargs = {}

    imshow_kwargs_final = {
        'cmap': 'jet',
        'interpolation': 'nearest',
        'origin': 'upper',
    }
    imshow_kwargs_final.update(imshow_kwargs)

    if data.ndim == 2:
        # Simply ignore the stat function in this case
        ax = fig.add_subplot(111)
        ax.imshow(data, **imshow_kwargs_final)
        if show_colorbar:
            plt.colorbar(**cbar_kwargs)
        ax.set_aspect('equal')
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        if contour_data is not None:
            if isinstance(contour_data, dict):
                for key, cdata in contour_data.items():
                    cs = ax.contour(cdata, levels=[0.5, ], colors=['k', ])
                    # ax.clabel(cs, cs.levels,
                    #           inline=1, fmt={0.5:str(key)}, fontsize=10)
            else:
                ax.contour(contour_data, levels=[0.5, ], colors=['k', ])
    elif (data.ndim == 3) and (stat_function is not None):
        ax = fig.add_subplot(111)
        ax.imshow(stat_function(data, **stat_kwargs), **imshow_kwargs_final)
        ax.set_aspect('equal')
        if show_colorbar:
            plt.colorbar(**cbar_kwargs)
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        if contour_data is not None:
            if isinstance(contour_data, dict):
                for key, cdata in contour_data.items():
                    cs = ax.contour(cdata, levels=[0.5, ], colors=['k', ])
                    # ax.clabel(cs, cs.levels, inline=1,
                    #           fmt={0.5:str(key)}, fontsize=10)
            else:
                ax.contour(contour_data, levels=[0.5, ], colors=['k', ])
    elif (data.ndim == 3) and (stat_function is None):
        num = data.shape[0]
        if num == 10:
            gridspec = (2, 5)
        else:
            rowcol = int(np.ceil(np.sqrt(num)))
            gridspec = (rowcol, rowcol)
        grid = AxesGrid(fig, 111,
                        nrows_ncols=gridspec,
                        axes_pad=0.05,
                        **axesgrid_extra_kwargs
                        )

        imshows = []
        if show_colorbar == "single":
            # Use same scale for all
            vmin = np.min(data)
            vmax = np.max(data)
        else:
            vmin = None
            vmax = None
        if "vmin" not in imshow_kwargs_final:
            imshow_kwargs_final["vmin"] = vmin
        if "vmax" not in imshow_kwargs_final:
            imshow_kwargs_final["vmax"] = vmax

        for i in range(num):
            im = grid[i].imshow(data[i], **imshow_kwargs_final)
            imshows.append(im)
            grid[i].set_aspect('equal')
            at = AnchoredText(
                "z=%d" % i,
                loc=2,
                prop=dict(size=8, color='w'),
                frameon=False,
            )
            grid[i].add_artist(at)
            if contour_data is not None:
                if isinstance(contour_data, dict):
                    for key, cdata in contour_data.items():
                        cs = ax.contour(cdata[i], levels=[
                                        0.5, ], colors=['k', ])
                        # ax.clabel(cs, cs.levels, inline=1,
                        #           fmt={0.5:str(key)}, fontsize=10)
                else:
                    grid[i].contour(contour_data[i], levels=[
                                    0.5, ], colors=['k', ])
            if plot_individual:
                ftemp = plt.figure()
                atemp = ftemp.add_subplot(111)
                atemp.imshow(data[i], **imshow_kwargs_final)
                atemp.set_aspect('equal')
                at = AnchoredText(
                    "z=%d" % i,
                    loc=2,
                    prop=dict(size=8, color='w'),
                    frameon=False,
                )
                atemp.add_artist(at)
                if contour_data is not None:
                    if isinstance(contour_data, dict):
                        for key, cdata in contour_data.items():
                            cs = atemp.contour(cdata[i], levels=[
                                               0.5, ], colors=['k', ])
                            atemp.clabel(cs, cs.levels, inline=1, fmt={
                                         0.5: str(key)}, fontsize=10)
                    else:
                        atemp.contour(contour_data[i], levels=[
                                      0.5, ], colors=['k', ])
                atemp.get_xaxis().set_visible(False)
                atemp.get_yaxis().set_visible(False)
                if filename:
                    savedfiles = savefig(
                        ftemp,
                        "%s_individual_%0.3d" % (filename, i),
                        output_path, output_types
                    )
                    if archive:
                        put_in_archive(savedfiles, archive)
                if close:
                    plt.close(ftemp)

        for i in range(np.prod(gridspec)):
            grid[i].get_xaxis().set_visible(False)
            grid[i].get_yaxis().set_visible(False)
            if show_colorbar == "multiple":
                grid.cbar_axes[i].colorbar(imshows[i], **cbar_kwargs)
        if show_colorbar == "single":
            grid.cbar_axes[0].colorbar(imshows[-1], **cbar_kwargs)
    elif data.ndim == 4:
        num = data[0].shape[0]
        if num == 10:
            gridspec = (2, 5)
        else:
            rowcol = int(np.ceil(np.sqrt(num)))
            gridspec = (rowcol, rowcol)
        grid = AxesGrid(fig, 111,
                        nrows_ncols=gridspec,
                        axes_pad=0.05,
                        **axesgrid_extra_kwargs
                        )
        imshows = []
        if show_colorbar == "single":
            # Use same scale for all
            vmin = np.min([stat_function(data[:, i, ...], **stat_kwargs)
                           for i in range(num)])
            vmax = np.max([stat_function(data[:, i, ...], **stat_kwargs)
                           for i in range(num)])
        else:
            vmin = None
            vmax = None
        if "vmin" not in imshow_kwargs_final:
            imshow_kwargs_final["vmin"] = vmin
        if "vmax" not in imshow_kwargs_final:
            imshow_kwargs_final["vmax"] = vmax

        for i in range(num):
            im = grid[i].imshow(
                stat_function(data[:, i, ...], **stat_kwargs),
                **imshow_kwargs_final)
            imshows.append(im)
            grid[i].set_aspect('equal')
            at = AnchoredText(
                "z=%d" % i,
                loc=2,
                prop=dict(size=8, color='w'),
                frameon=False,
            )
            grid[i].add_artist(at)
            if contour_data is not None:
                if isinstance(contour_data, dict):
                    for key, cdata in contour_data.items():
                        cs = grid[i].contour(
                            cdata[i], levels=[0.5, ], colors=['k', ])
                        # grid[i].clabel(cs, cs.levels, inline=1,
                        #                fmt={0.5:str(key)}, fontsize=10)
                else:
                    grid[i].contour(contour_data[i], levels=[
                                    0.5, ], colors=['k', ])

            if plot_individual:
                ftemp = plt.figure()
                atemp = ftemp.add_subplot(111)
                atemp.imshow(stat_function(data[:, i, ...], **stat_kwargs),
                             **imshow_kwargs_final)
                atemp.set_aspect('equal')
                at = AnchoredText(
                    "z=%d" % i,
                    loc=2,
                    prop=dict(size=8, color='w'),
                    frameon=False,
                )
                atemp.add_artist(at)
                if contour_data is not None:
                    if isinstance(contour_data, dict):
                        for key, cdata in contour_data.items():
                            cs = atemp.contour(cdata[i], levels=[
                                               0.5, ], colors=['k', ])
                            atemp.clabel(cs, cs.levels, inline=1, fmt={
                                         0.5: str(key)}, fontsize=10)
                    else:
                        atemp.contour(contour_data[i], levels=[
                                      0.5, ], colors=['k', ])
                atemp.get_xaxis().set_visible(False)
                atemp.get_yaxis().set_visible(False)
                if filename:
                    savedfiles = savefig(
                        ftemp,
                        "%s_individual_%0.3d" % (filename, i),
                        output_path, output_types)
                    if archive:
                        put_in_archive(savedfiles, archive)
                if close:
                    plt.close(ftemp)

        for i in range(np.prod(gridspec)):
            grid[i].get_xaxis().set_visible(False)
            grid[i].get_yaxis().set_visible(False)
            if show_colorbar == "multiple":
                grid.cbar_axes[i].colorbar(imshows[i], **cbar_kwargs)
        if show_colorbar == "single":
            grid.cbar_axes[0].colorbar(imshows[-1], **cbar_kwargs)
    if filename:
        savefig(fig, filename, output_path, output_types)
    if close:
        plt.close(fig)
    if log_description:
        logger.info("OUTPUT: %s  -  %s" % (filename, log_description)),


def plot_lines(
        lines,
        NLines=1000,
        filename="",
        update_function=default_update_function,
        output_path=None,
        output_types=[],
        close=True,
        log_description="",
        ):
    fig = plt.figure()
    x = np.arange(len(lines[0]))
    N = lines.shape[0]
    inds = np.arange(lines.shape[0])
    if N > NLines:
        N = NLines
        np.random.shuffle(inds)  # NOTE: this is in-place
        inds = inds[:N]
        update_function(
            0, N,
            message="Plotting %d randomly selected lines (of %d)..." % (
                N, lines.shape[0]
            )
        )
    else:
        update_function(0, N, message="Plotting %d lines ..." % N)

    ax = fig.add_subplot(111)
    for ni, i in enumerate(inds):
        ax.plot(x, lines[i])
        update_function(n=ni, N=N)
    update_function(message="Done plotting lines")
    if filename:
        savefig(fig, filename, output_path, output_types)
    if close:
        plt.close(fig)
    if log_description:
        logger.info("OUTPUT: %s  -  %s" % (filename, log_description)),
