#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Testing utility functions
#
import random
import string

def do_nothing(*args, **kwargs):
    pass

def random_string(nchars):
    return "".join(random.choice(string.ascii_letters) for i in range(nchars))
