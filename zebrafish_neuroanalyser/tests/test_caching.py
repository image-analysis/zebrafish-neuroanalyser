#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Test IO functions
#

import unittest
from unittest import mock
import zebrafish_neuroanalyser.dataio as zio
from zebrafish_neuroanalyser.tests.utility import do_nothing, random_string
import logging
import zipfile
import numpy as np
import builtins
import sys
import io
import json
import os
import random

class Test_load_aligned_rois_from_file(unittest.TestCase):
    def setUp(self):
        self.filenamesEmpty = []
        self.filenamesOK = ["a", "b", "c"]
        self.selectedEmpty = []
        self.selectedOK = [1,2,3]
        self.affine = []
        self.shift = []
        self.n_rois = 10,
        self.roifilebase = "dummy%d"
        self.emptyfile = io.StringIO()

        self.jsonfile_empty_dict= io.StringIO()
        self.jsonfile_empty_dict.write("{}")
        self.jsonfile_empty_dict.seek(0)

        self.metadatafile_mismatch = io.StringIO()
        self.metadatafile_mismatch.write('{"something":["d","e","f"]}')
        self.metadatafile_mismatch.seek(0)

        self.metadatafile_OK = io.StringIO()
        self.metadatafile_OK.write('{"filenames":["a","b","c"]}')
        self.metadatafile_OK.seek(0)

        self.metadatafile_OK_with_roifilebase = io.StringIO()
        self.metadatafile_OK_with_roifilebase.write(
            """{
                "filenames":["a","b","c"],
                "roifilebase":"dummy%d",
                "selected":[]
            }""")
        self.metadatafile_OK_with_roifilebase.seek(0)

        self.metadatafile_OK_with_roifilebase_and_selected = io.StringIO()
        self.metadatafile_OK_with_roifilebase_and_selected.write(
            """{
                "filenames":["a","b","c"],
                "roifilebase":"dummy%d",
                "selected":[1,2,3]
            }""")
        self.metadatafile_OK_with_roifilebase_and_selected.seek(0)

        # Copied from function code to probe inner workings better
        outfolder = "%s_intermediate"%self.filenamesOK[0]
        self.metadata_file = os.path.join(outfolder, "roi_parameters.json")
        self.roinames_file = os.path.join(outfolder, "roi_names.json")
        self.roi_file = os.path.join(outfolder, "aligned_rois.zip")


    def test_no_input(self):
        with self.assertRaises(IndexError):
            # Should try and access filenames[0]
            zio.load_aligned_rois_from_file()

    def test_empty_input(self):
        with self.assertRaises(IndexError):
            # Should try and access filenames[0]
            zio.load_aligned_rois_from_file(self.filenamesEmpty)

    @mock.patch('os.path.isfile')
    def test_basic_missing_file(
            self,
            mockisfile,
            ):
        # Should return (None, None) as metadatafile not present
        mockisfile.return_value = False
        res = zio.load_aligned_rois_from_file(self.filenamesOK)
        self.assertEqual((None, None), res)


    @mock.patch('os.path.isfile')
    def test_missing_metadata_file(
            self,
            mockisfile,
            ):
        # Should return (None, None) as metadatafile not present
        mockisfile.side_effect = lambda f: False if f==self.metadata_file else True
        res = zio.load_aligned_rois_from_file(self.filenamesOK)
        self.assertEqual((None, None), res)


    @mock.patch('os.path.isfile')
    def test_missing_roinames_file(
            self,
            mockisfile,
            ):
        # Should return (None, None) as metadatafile not present
        mockisfile.side_effect = lambda f: False if f==self.roinames_file else True
        res = zio.load_aligned_rois_from_file(self.filenamesOK)
        self.assertEqual((None, None), res)


    @mock.patch('os.path.isfile')
    def test_missing_roi_file(
            self,
            mockisfile,
            ):
        # Should return (None, None) as metadatafile not present
        mockisfile.side_effect = lambda f: False if f==self.roi_file else True
        res = zio.load_aligned_rois_from_file(self.filenamesOK)
        self.assertEqual((None, None), res)


    @mock.patch('os.path.isfile')
    @mock.patch('builtins.open')
    def test_basic_metadatafile_empty(
            self,
            mockopen,
            mockisfile,
            ):
        mockisfile.return_value = True
        mockopen.return_value = self.emptyfile
        with self.assertRaises(json.decoder.JSONDecodeError):
            res = zio.load_aligned_rois_from_file(self.filenamesOK)


    @mock.patch('os.path.isfile')
    @mock.patch('builtins.open')
    def test_metadatafile_ok_mismatch(
            self,
            mockopen,
            mockisfile,
            ):
        mockisfile.return_value = True
        mockopen.return_value = self.metadatafile_mismatch
        res = zio.load_aligned_rois_from_file(self.filenamesOK)
        self.assertEqual((None, None), res)


    @mock.patch('os.path.isfile')
    @mock.patch('builtins.open')
    def test_metadatafile_ok_no_keys(
            self,
            mockopen,
            mockisfile,
            ):
        mockisfile.return_value = True
        mockopen.side_effect = lambda f: \
            self.jsonfile_empty_dict if f==self.metadata_file \
            else self.emptyfile
        res = zio.load_aligned_rois_from_file(self.filenamesOK)
        self.assertEqual((None, None), res)
        self.assertEqual(mockopen.call_count, 1)


    @mock.patch('os.path.isfile')
    @mock.patch('builtins.open')
    def test_metadatafile_ok_good_metadata_empty_roifile(
            self,
            mockopen,
            mockisfile,
            ):
        """
        Should get past the metadata loading and check, but then
        fail on the roi loading
        """
        mockisfile.return_value = True
        mockopen.side_effect = lambda f: \
            self.metadatafile_OK if f==self.metadata_file \
            else self.emptyfile
        with self.assertRaises(json.decoder.JSONDecodeError):
            res = zio.load_aligned_rois_from_file(self.filenamesOK)
        self.assertEqual(mockopen.call_count, 2)


    @mock.patch('os.path.isfile')
    @mock.patch('builtins.open')
    @mock.patch('zipfile.ZipFile')
    def test_metadatafile_ok_good_metadata_no_roinames_no_selected(
            self,
            mockzipfile,
            mockopen,
            mockisfile,
            ):
        """
        Should get past the metadata loading and check, but then
        fail on the roi loading
        """
        mockisfile.return_value = True
        def mockopenreturn(f):
            if f == self.metadata_file:
                return self.metadatafile_OK_with_roifilebase
            elif f == self.roinames_file:
                return self.jsonfile_empty_dict
            else:
                return self.emptyfile
        mockopen.side_effect = mockopenreturn
        with mock.patch.object(zipfile.ZipFile, "open", return_value=None):
            res = zio.load_aligned_rois_from_file(self.filenamesOK,
                roifilebase=self.roifilebase,
                selected=self.selectedEmpty,
                )
        self.assertEqual(mockopen.call_count, 2)
        mockzipfile.assert_called_once_with(self.roi_file)


    @mock.patch('skimage.io.imread')
    @mock.patch('os.path.isfile')
    @mock.patch('builtins.open')
    @mock.patch('zipfile.ZipFile')
    def test_metadatafile_ok_good_metadata_no_roinames(
            self,
            mockzipfile,
            mockopen,
            mockisfile,
            mockskioimread,
            ):
        """
        Should get past the metadata loading and check, but then
        fail on the roi loading
        """
        mockisfile.return_value = True
        def mockopenreturn(f):
            if f == self.metadata_file:
                return self.metadatafile_OK_with_roifilebase_and_selected
            elif f == self.roinames_file:
                return self.jsonfile_empty_dict
            else:
                return self.emptyfile
        mockopen.side_effect = mockopenreturn
        mockskioimread.return_value = 1

        with mock.patch.object(zipfile.ZipFile, "open", return_value=None):
            res = zio.load_aligned_rois_from_file(self.filenamesOK,
                roifilebase=self.roifilebase,
                selected=self.selectedOK,
                )
        self.assertEqual(mockopen.call_count, 2)
        mockzipfile.assert_called_once_with(self.roi_file)
        self.assertEqual(mockskioimread.call_count, 3)
        self.assertEqual(
            res,
            ({name:True for name in self.selectedOK}, {})
        )
