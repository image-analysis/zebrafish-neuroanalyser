#
# FILE        : testing.py
# CREATED     : 13/10/15 12:12:46
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Test functions
#
import zebrafish_neuroanalyser.main as main
import glob
import cProfile
import time

import multiprocessing
import os
import sys
import tempfile

import string

import numpy as np
##=========================================================
## Utility eg loading
##=========================================================
##=========================================================
## Testdata
##=========================================================

def generate_test_data(
        T = 50,
        W = 100,
        H = 100,
        Z = 9,
        NOISE = 0.1,
        SIG = 100,
        SIGNOISE = 10,
        nregions = 8,
        ):
    """
    Generate sample data for quick testing
    """
    data = NOISE * SIG * np.random.rand(T,Z,W,H)
    # Brain - bounds
    x0 = int(2*W/10)
    x1 = int(8*W/10)
    y0 = int(2*H/10)
    y1 = int(8*H/10)
    dx = (x1-x0)/7
    dy = (y1-y0)/7
    z0 = 1
    z1 = Z-1
    data[ : , z0:z1, x0:x1, y0:y1 ] = SIG + SIGNOISE*np.random.randn(
        T, z1-z0, x1-x0, y1-y0)
    # Create regions
    for n in range(nregions):

        xx0     = x0 + 2* (n % 3) * dx + dx
        xx1     = xx0 + dx
        yy0     = y0 + 2* int(n/3) * dy + dy
        yy1     = yy0  + dy
        zz0     = np.random.randint(z0, z1-1)
        zz1     = np.random.randint(zz0+1, z1)
        taxis   = np.arange(T)[:,np.newaxis, np.newaxis, np.newaxis]
        # Using simple sinusoids
        #phase   = 10*np.random.rand()
        #amp     = 0.4* np.random.rand()
        #freq    = 0.2*(3 + np.random.rand())/4
        #data[:, zz0:zz1, xx0:xx1, yy0:yy1] += SIG*amp*np.sin(
        #    freq * taxis  + phase)[:, np.newaxis, np.newaxis, np.newaxis]

        # Using a gaussian peak of activity
        tmax    = T * ((float(n+1)/(nregions+1)) + 0.1*np.random.randn()) # in 0.1-0.9 T
        #amp     = SIG * (5 + 5* np.random.rand()) # 5-10 SIG
        amp     = 5 + 5* np.random.rand() # 5-10 SIG
        width   = 1 + 2* np.random.rand() # 2-5 width
        data[:, zz0:zz1, xx0:xx1, yy0:yy1] *= (1  + amp*np.exp(
            -(taxis-tmax)*(taxis-tmax) / (width*width)))
        # Randomly add a small gradient on half the clusters
        if np.random.rand() < 0.5:
            data[:, zz0:zz1, xx0:xx1, yy0:yy1] += 4*SIG*(taxis.astype(float)/taxis.max())

    return data.astype(int)

def load_sample_data_0(n=None, align_on_load=True, sample_factor=[3,3]):
    DATADIR = "/data/new_data_root/jeremy/201509/thunder_test/4dpf_Brain_Imaging_15step_PTZ-5mm_20exp_130slice_3"
    filenames = main.sort_filenames( glob.glob("%s/*tif"%DATADIR), time_regex = "TL(\d+)"  )
    #print("Found %d sample files:")
    #print("\n".join(filenames))
    if n is not None:
        filenames = filenames[:n]
    return main.load_and_resample(filenames,sample_factor=sample_factor, align_on_load=align_on_load)


def load_sample_data_1(n=None, align_on_load=True, sample_factor=[3,3]):
    DATADIR = "/data/new_data_root/dw388/201605/new_fls_data/15 slices no BF"
    filename = os.path.join(DATADIR, "spim_TL00_Angle0.ome.tiff")
    return main.load_and_resample(filename,sample_factor=sample_factor, align_on_load=align_on_load)


def load_sample_data(n=None, align_on_load=True, sample_factor=[3,3]):
    DATADIR = "/data/new_data_root/dw388/201605/newer_fls_data/Stacks for registration/Fish 1/BF"
    filename = os.path.join(DATADIR, "spim_TL00_Angle0.ome.tiff")
    return main.load_and_resample(filename,sample_factor=sample_factor, align_on_load=align_on_load)
##=========================================================
## Tests
##=========================================================

#------------------------------
# Registration
#------------------------------

def test_registration(N=None):
    print("Testing registration")
    print("\tLoading data without aligning...")
    t0 = time.time()
    data = load_sample_data(n=N, align_on_load=False)
    dt = time.time() - t0
    num = data.shape[0]
    print("Took %f seconds, ie %f s/frame"%(dt, dt/num))
    print("Loaded into array: %s"%str(data.shape))
    print("\tLoading data with aligning...")
    t0 = time.time()
    data2 = load_sample_data(n=N, align_on_load=True)
    dt = time.time() - t0
    num = data.shape[0]
    print("Took %f seconds, ie %f s/frame"%(dt, dt/num))
    #data2 = main.align_stack(data, method='running_mean', update_function=main.default_update_function, verbose=True)
    print("\tVisualizing output")
    # Now show videos of the original data and the running mean
    #from jm_packages.image_processing import guis as jmgui
    #imapp = jmgui.PGImageViewer()
    #jmgui.imshow_pg([data.max(axis=1).squeeze(), data2.max(axis=1).squeeze()])

    # Use matplotlib animation to generate avi
    import matplotlib.animation as animation
    import matplotlib.pyplot as pp
    def datatompg(
            data,
            filename,
            dpi=100):
        fig = pp.figure()
        ax = fig.add_subplot(111)
        ax.set_aspect('equal')
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        def get_frame(dataslice):
            return dataslice.max(axis=0)

        frame0 = get_frame(data[0])
        im = ax.imshow( frame0,cmap='gray',interpolation='nearest')
        imin, imax = frame0.min(), frame0.max()
        im.set_clim([imin, imax])
        pp.tight_layout()

        def update_frame(n):
            im.set_data(get_frame(data[n]))
            return im
        ani = animation.FuncAnimation(fig, update_frame, frames=data.shape[0])
        writer = animation.writers['gif'](fps=30)
        ani.save(filename, writer=writer, dpi=dpi)
        return ani
    datatompg(data, "original.gif")
    datatompg(data2, "aligned.gif")


def profile_registration(n_frames=10):
    pr = cProfile.Profile()
    print("Profiling registration")
    print("\tLoading data...")
    pr.enable()
    data = load_sample_data(n=n_frames)
    pr.disable()
    pr.dump_stats("aligning_stack_profile_%d_frames.prof"%n_frames)


def _umeyama(src, dst, estimate_scale):
    """
    Copied from https://github.com/scikit-image/scikit-image/blob/master/skimage/transform/_geometric.py#L71
    Estimate N-D similarity transformation with or without scaling.
    Parameters
    ----------
    src : (M, N) array
        Source coordinates.
    dst : (M, N) array
        Destination coordinates.
    estimate_scale : bool
        Whether to estimate scaling factor.
    Returns
    -------
    T : (N + 1, N + 1)
        The homogeneous similarity transformation matrix. The matrix contains
        NaN values only if the problem is not well-conditioned.
    References
    ----------
    .. [1] "Least-squares estimation of transformation parameters between two
            point patterns", Shinji Umeyama, PAMI 1991, DOI: 10.1109/34.88573
    """

    num = src.shape[0]
    dim = src.shape[1]

    # Compute mean of src and dst.
    src_mean = src.mean(axis=0)
    dst_mean = dst.mean(axis=0)
    print("UMEYAMA: Means of source and dest")
    print(src_mean)
    print(dst_mean)

    # Subtract mean from src and dst.
    # Eq (34) & (35)
    src_demean = src - src_mean
    dst_demean = dst - dst_mean

    # Eq. (38). (called $/Sigma_{xy}$)
    A = np.dot(dst_demean.T, src_demean) / num

    # Eq. (39). CALLED S
    d = np.ones((dim,), dtype=np.double)
    if np.linalg.det(A) < 0:
        d[dim - 1] = -1

    T = np.eye(dim + 1, dtype=np.double)

    U, S, V = np.linalg.svd(A)

    # Eq. (40) and (43).
    # R = U * S * V.T
    rank = np.linalg.matrix_rank(A)
    if rank == 0:
        return np.nan * T
    elif rank == dim - 1:       # Paper has rank >= m-1
        if (np.linalg.det(U) * np.linalg.det(V)) > 0:
            # As S = identity if det(U).det(V) = 1
            T[:dim, :dim] = np.dot(U, V.T)
        else:
            # Else S = diag(1,1,1,..,-1)
            s = d[dim - 1]
            d[dim - 1] = -1
            # i.e. T is R, d is S
            T[:dim, :dim] = np.dot(U, np.dot(np.diag(d), V.T))
            d[dim - 1] = s
    else:
        #print("\n\n")
        #print("DOING THIS")
        #print("DET(U).DET(V)", np.linalg.det(U) * np.linalg.det(V))
        #print("DIAG THING:", np.diag(d))
        #print("\n\n")
        T[:dim, :dim] = np.dot(U, np.dot(np.diag(d), V.T))

    if estimate_scale:
        # Eq. (41) and (42).
        scale = 1.0 / src_demean.var(axis=0).sum() * np.dot(S, d)
    else:
        scale = 1.0

    #print("UMEYAMA: Calculating shift:")
    #print("dst_mean:", dst_mean)
    #print("src_mean:",  src_mean)
    #print("Scale:", scale)
    #print("TRANSFORM . SRC_MEAN",  np.dot(T[:dim, :dim], src_mean.T))

    T[:dim, dim] = dst_mean - scale * np.dot(T[:dim, :dim], src_mean.T)
    T[:dim, :dim] *= scale

    return T

#------------------------------
# Plotting
#------------------------------

def test_plotting(N=100):
    import matplotlib.pyplot as pp
    pp.switch_backend('qt4agg')
    import numpy as np
    data = load_sample_data(n=N, align_on_load=False)
    # Test max
    main.plot_data(data, filename=None, stat_function = np.max, stat_kwargs=dict(axis=0), close=False)
    lines = main.data_to_profiles(data)
    main.plot_lines(lines, close=False)
    pp.show()


#------------------------------
# Using label data from ZBrain
#------------------------------

def test_loading_label_data(
        labelfile = "/home/jeremy/Downloads/MaskDatabase.mat",
        ):
    import h5py
    f = h5py.File(labelfile)
    W = int(f['width'].value[0, 0])
    H = int(f['height'].value[0, 0])
    Z = int(f['Zs'].value[0, 0])

    # Extract the sparse matrix components
    print(list(f.keys()))
    print("MaskDatabaseNames")
    print(len(f['MaskDatabaseNames']))
    print(f['MaskDatabaseNames'][0][0])
    name = f[f['MaskDatabaseNames'][0][0]].value
    print( "".join((chr(n) for n in name)))
    names = []
    for ref in f['MaskDatabaseNames']:
        name = f[ref[0]].value
        names.append("".join((chr(n) for n in name)))
    print("MaskDatabase")
    for ref in f['MaskDatabase']:
        print(ref)
    print("MaskDatabaseOutlines")
    for ref in f['MaskDatabaseOutlines']:
        print(ref)

    outlines = f['MaskDatabaseOutlines']
    data = outlines['data'].value
    ir = outlines['ir'].value
    jc = outlines['jc'].value

    # Try reconstituting
    label_flat = np.zeros(W*H*Z, dtype='int32')
    print("Created empty label array", label_flat.shape)
    label_flat[ir] = data
    print("Populated label array, reshaping...")
    # Options here for order are C, F, or A
    label = np.reshape(label_flat, (Z,W,H), order="C")
    labelz = label * (1+ np.arange(Z)[:,None,None])

    print("Label: ", label.shape, "min & max:", label.min(), label.max())

    import matplotlib.pyplot as plt
    plt.switch_backend('agg')
    plt.imshow(labelz.max(axis=0))
    plt.colorbar()
    plt.savefig('test_zbrain_labels_z_scaled.png')
    plt.close()
    plt.imshow(label.max(axis=0))
    plt.colorbar()
    plt.savefig('test_zbrain_labels.png')
    plt.close()

    # now assuming that jc indexes into ir
    for i in range(len(jc)-1):
        label_flat = np.zeros(W*H*Z, dtype='int32')
        label_flat[ir[jc[i]:jc[i+1]]] = data[jc[i]:jc[i+1]]
        # Options here for order are C, F, or A
        label = np.reshape(label_flat, (Z,W,H), order="C")
        labelz = label * (1+ np.arange(Z)[:,None,None])
        plt.imshow(labelz.max(axis=0))
        plt.colorbar()
        name = names[i] if (i < len(names)) else "DUNNO"
        plt.savefig('test_zbrain_labels_z_scaled_%0.4d_%s.png'%(i, name.replace('/','_OF_')))
        plt.close()


def load_label_data_old(
        labelfile = "/home/jeremy/Downloads/MaskDatabase.mat",
        ):
    print("Loading label data from Matlab file...(using h5py)")
    sys.stdout.flush()
    import h5py
    f = h5py.File(labelfile)
    W = int(f['width'].value[0, 0])
    H = int(f['height'].value[0, 0])
    Z = int(f['Zs'].value[0, 0])

    # Extract the sparse matrix components
    outlines = f['MaskDatabaseOutlines']
    data = outlines['data'].value.copy()
    ir = outlines['ir'].value.copy()
    jc = outlines['jc'].value.copy()

    # Try reconstituting
    label_flat = np.zeros(W*H*Z, dtype='int32')
    label_flat[ir] = data
    # Options here for order are C, F, or A
    label = np.reshape(label_flat, (Z,W,H), order="C")

    if True:
        print("Iterating over %d labels..."%len(jc))
        sys.stdout.flush()
        for i in range(len(jc)-1):
            label_flat = np.zeros(W*H*Z, dtype='int32')
            label_flat[ir[jc[i]:jc[i+1]]] = data[jc[i]:jc[i+1]]
            # Options here for order are C, F, or A
            label_now = np.reshape(label_flat, (Z,W,H), order="C")
            label[label_now>0] = i+1
            sys.stdout.write("x")
            sys.stdout.flush()
    label = label.transpose([0,2,1])
    return label


def load_label_data(
        labeldir = "/home/jeremy/Downloads/MaskDatabase.mat_labels",
        ):
    print("Loading label data from tiff files...")
    sys.stdout.flush()
    files = sorted(glob.glob(os.path.join(labeldir, "*tif")))
    print("Iterating over %d labels..."%len(files))
    sys.stdout.flush()
    label = np.zeros(main.skio.imread(files[0]).shape, dtype="int32")
    for i, f in enumerate(files):
        label_now = main.skio.imread(f)
        label[label_now>0] = i+1
        sys.stdout.write("x")
        sys.stdout.flush()
    #label = label.transpose([0,2,1])
    return label

def test_register_against_label_data(
        labelfile = "/home/jeremy/Downloads/MaskDatabase.mat",
        do_reshape = False,
    ):

    #sample_data = load_sample_data_0(n=2, sample_factor=[1,1])
    #sample_data = load_sample_data_1(n=2, sample_factor=[1,1])
    sample_data = load_sample_data(n=2, sample_factor=[1,1])
    labeldata   = load_label_data()

    print("Loaded sample data:", sample_data.shape)
    print("Loaded label data:", labeldata.shape)

    im0 = labeldata.max(axis=0)
    im1 = sample_data.max(axis=0).max(axis=0)

    im0 = im0.T.copy(order="C")

    if do_reshape:
        diffs = [ s0 - s1 for s0, s1 in zip( im0.shape, im1.shape) ]
        print(diffs)
        pads = [ (d//2, d//2 + d%2) if d>0 else (0,0) for d in diffs]
        im2 = np.pad(im1, pads, "edge")
        # now pad template if needed
        diffs = [ s1 - s0 for s0, s1 in zip(im0.shape, im2.shape)]
        pads  = [ (d//2, d//2 + d%2) if d>0 else (0,0) for d in diffs]
        im0 = np.pad(im0, pads, "edge")
    else:
        im2 = im1.copy(order="C")

    import matplotlib.pyplot as plt
    plt.switch_backend('agg')
    plt.imshow(im0)
    plt.colorbar()
    plt.savefig('test_reg_zbrain_labels.png')
    plt.close()
    plt.imshow(im1)
    plt.colorbar()
    plt.savefig('test_reg_data.png')
    plt.close()
    plt.imshow(im2)
    plt.colorbar()
    plt.savefig('test_reg_data_resized.png')
    plt.close()

    # Lets register the template onto the data
    if True:
        import imreg_dft as ird
        res = ird.similarity(im0, im2, numiter=3)
        #print(res.keys())
        ird.imshow(im0, im2, res['timg'])
        plt.savefig("test_reg_result.png")
    if False:
        # Try the other imreg module
        import imreg.register as reg
        import imreg.model as model
        affine = reg.Register()
        i0 = reg.RegisterData(im0)
        i1 = reg.RegisterData(im2)
        # could add e.g. method = metric.forwardAdditive
        step, _search = affine.register(i0, i1, model.Affine())
        print("TRANSFORM FROM IMREG:")
        print(step.p)



def test_register_against_ref_data(
        reffile = "/home/jeremy/Downloads/Ref20131120pt14pl2.tif",
    ):
    sample_data = load_sample_data(n=2, sample_factor=[1,1])
    refdata   = main.skio.imread(reffile)

    print("Loaded sample data (including resampling AFAIK):", sample_data.shape)
    print("Loaded reference data:", refdata.shape)

    im0 = refdata.max(axis=0)
    im1 = sample_data.max(axis=0).max(axis=0)

    #im1 = im1.T

    diffs = [ s0 - s1 for s0, s1 in zip( im0.shape, im1.shape) ]
    print(diffs)
    pads = [ (d//2, d//2 + d%2) if d>0 else (0,0) for d in diffs]
    im2 = np.pad(im1, pads, "edge")
    # now pad template if needed
    diffs = [ s1 - s0 for s0, s1 in zip(im0.shape, im2.shape)]
    pads  = [ (d//2, d//2 + d%2) if d>0 else (0,0) for d in diffs]
    im0 = np.pad(im0, pads, "edge")


    import matplotlib.pyplot as plt
    plt.switch_backend('agg')
    plt.imshow(im0)
    plt.colorbar()
    plt.savefig('test_reg_zbrain_ref.png')
    plt.close()
    plt.imshow(im1)
    plt.colorbar()
    plt.savefig('test_reg_data.png')
    plt.close()
    plt.imshow(im2)
    plt.colorbar()
    plt.savefig('test_reg_data_resized.png')
    plt.close()

    # Lets register the template onto the data
    import imreg_dft as ird
    res = ird.similarity(im0, im2, numiter=3)
    print(res.keys())
    ird.imshow(im0, im2, res['timg'])
    plt.savefig("test_reg_ref_result.png")


def roifile2coords_old(fname):
    bname = os.path.splitext(os.path.basename(fname))[0]
    coords = np.array([int(part) for part in bname.split('__')])
    return coords

def roifile2coords(fname):
    """
    New version: use roi files directly
    """
    bname = os.path.splitext(os.path.basename(fname))[0]
    coords = np.array([int(part) for part in bname.split('-')[1:]])
    return coords


def load_roi_set(
        path="/data/new_data_root/dw388/201607/ROI Info/ZF Brain Reference ROIs",
        ext="roi",
        #ext="txt",
        ):
    roifiles = sorted([f for f in os.listdir(path) if f.endswith('.%s'%ext)])
    # X Y Z encoded in filename...
    coords = np.array([ roifile2coords(f) for f in roifiles ])
    # TODO:Subtract 1 from z?
    return coords


def convert_rois(path):
    print("LOADING ROIS FROM")
    print(path)
    pts = load_roi_set(path)
    print("LOADED:")
    print(pts)
    print("CONVERTING...")
    outfile = os.path.join(path, "rois.txt")
    if os.path.isfile(outfile):
        print("ROI FILE ALREADY EXISTS:")
        with open(outfile) as fid:
            for l in fid:
                print(l)
        print("EXITING...")
        return
    with open(outfile, "w") as fid:
        for lett, p in zip(string.lowercase, pts):
            pnow = [p[1], p[2], p[0]]
            fid.write("%s\n"%",".join([lett,] + ["%0.2f"%v for v in pnow]))


def test_load_roi_set(root="/data/new_data_root/dw388/201607/ROI Info/"):
    for r, paths, files in os.walk(root):
        paths = [os.path.join(r, p) for p in paths]
        break
    # Get the corresponding data
    datafiles  = []
    for p in paths:
        if os.path.isfile(p[:-5]):
            datafiles.append(p[:-5])

    import matplotlib.pyplot as plt
    plt.switch_backend('qt4agg')
    DO3D = 0
    DOSHOWPOINTS = 0
    TRYREG = 0

    if DO3D:
        from mpl_toolkits.mplot3d import Axes3D
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        cols=['r', 'g', 'b']
        for path, col in zip(paths, cols):
            print(path)
            coords = load_roi_set(path)
            # Lets see them z-projected
            ax.plot(coords[:,0], coords[:,1], coords[:,2], 'o%s'%col)
        plt.show()

    if DOSHOWPOINTS:
        for  path  in paths:
            # Get the corresponding data file
            datafile= path[:-5]
            # Load the coords
            coords = load_roi_set(path)
            # Load the data
            data = main.skio.imread(datafile)
            #print("Loaded: ", data.shape)
            while data.ndim > 2:
                data = data.max(axis=0)
            # Now try and show
            plt.imshow(data, cmap='gray')
            for l, p in zip(string.lowercase, coords):
                plt.plot(p[2], p[1], '.r')
                plt.text(p[2]+4, p[1]+4, l, color="y")
            plt.savefig("%s_points.png"%datafile)
            plt.close()
        return

    if TRYREG:
        # Now test a registraton from the reference to the sample
        import  jm_packages.tracking.external.gmmreg as gmmreg
        pathref = [p for p in paths if "reference" in p.lower()]
        #otherpaths = list(set(paths) - set(pathref))
        pathctrl = [p for p in paths if "control" in p.lower()]

        if len(pathref) != 1:
            raise Exception("Ooops")
        if len(pathctrl) != 1:
            raise Exception("Ooops")
        pathref =pathref[0]
        pathctrl =pathctrl[0]

        pts_ref = load_roi_set(pathref)
        pts_ctrl = load_roi_set(pathctrl)

        # TODO: May need manual hack afterall?
        pts_ctrl = np.array([pts_ctrl[:,0], 1000-pts_ctrl[:,2], pts_ctrl[:,1]]).T

        pts_ref2 = gmmreg.register(pts_ref, pts_ctrl)
        from mpl_toolkits.mplot3d import Axes3D
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(pts_ref[:,2], pts_ref[:,1], pts_ref[:,0], 'og')
        ax.plot(pts_ref2[:,2], pts_ref2[:,1], pts_ref2[:,0], '.r')
        ax.plot(pts_ctrl[:,2], pts_ctrl[:,1], pts_ctrl[:,0], 'ob')
        for p1, p2 in zip(pts_ref, pts_ref2):
            ax.plot([p1[2], p2[2]], [p1[1], p2[1]], [p1[0], p2[0]], '-y')
        plt.show()


    pathref = [p for p in paths if "reference" in p.lower()]
    #otherpaths = list(set(paths) - set(pathref))
    pathctrl = [p for p in paths if "control" in p.lower()]

    if len(pathref) != 1:
        raise Exception("Ooops")
    if len(pathctrl) != 1:
        raise Exception("Ooops")
    pathref =pathref[0]
    pathctrl =pathctrl[0]

    pts_ref = load_roi_set(pathref)
    pts_ctrl = load_roi_set(pathctrl)

    # Load control and ref data
    datapathref = [p for p in datafiles if "reference" in p.lower()][0]
    datapathctrl = [p for p in datafiles if "control" in p.lower()][0]
    #
    dataref = main.skio.imread(datapathref)
    datactrl = main.skio.imread(datapathctrl)
    # FIXME:
    # Pts ref - try flipping x axis, as we seem to have a flipping
    # going on that might be messing up the affine transform
    #pts_ref[:,1] = dataref.shape[1] - pts_ref[:,1]

    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(pts_ref[:,2], pts_ref[:,1], pts_ref[:,0], 'og')
    ax.plot(pts_ctrl[:,2], pts_ctrl[:,1], pts_ctrl[:,0], 'ob')
    for p1, p2 in zip(pts_ref, pts_ctrl):
        ax.plot([p1[2], p2[2]], [p1[1], p2[1]], [p1[0], p2[0]], '-y')

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(pts_ref[:,2], pts_ref[:,1], 'og')
    ax.plot(pts_ctrl[:,2], pts_ctrl[:,1], 'ob')
    for p1, p2 in zip(pts_ref, pts_ctrl):
        ax.plot([p1[2], p2[2]], [p1[1], p2[1]], '-y')

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_title("ROTATED REF")
    pts_ref_rot = pts_ref.copy()
    cent = pts_ref_rot.mean(axis=0).astype(int)
    pts_ref_rot -= cent
    pts_ref_rot = pts_ref_rot[:,[0, 2, 1]]
    pts_ref_rot[:,2] *= -1
    pts_ref_rot = (pts_ref_rot.astype(float)*0.6).astype(int)
    pts_ref_rot += pts_ctrl.mean(axis=0).astype(int)
    ax.plot(pts_ref_rot[:,2], pts_ref_rot[:,1], 'og')
    ax.plot(pts_ctrl[:,2], pts_ctrl[:,1], 'ob')
    for p1, p2 in zip(pts_ref_rot, pts_ctrl):
        ax.plot([p1[2], p2[2]], [p1[1], p2[1]], '-y')

    # Add in least squares matching based transform estimation

    #mat_affine = _umeyama(pts_ref, pts_ctrl,
    #mat_affine = _umeyama(pts_ctrl, pts_ref,
    #    estimate_scale=True,
    #    #estimate_scale=False,
    #    )

    # Try 2d
    print("POINTS SHAPE")
    print(pts_ctrl.shape)
    mat_affine = np.diag(np.ones(4))
    #matt2d = _umeyama(pts_ref[:,1:], pts_ctrl[:,1:],
    matt2d = _umeyama(pts_ctrl[:,1:], pts_ref[:,1:],
        estimate_scale=True,
    )
    mat_affine[1:,1:] = matt2d

    print("ON Z:")
    aff_z = _umeyama(pts_ctrl[:,0][:,None], pts_ref[:,0][:,None],
        estimate_scale=True,
    )

    print("MATCHING MATRIX:")
    print(mat_affine)
    #print("\n".join(datafiles))

    # Show points
    print("reference points:")
    print(pts_ref)
    print("control points")
    print(pts_ctrl)

    # Get 1st frame of timeseries
    datactrl = datactrl[0]
    print("Ref:", dataref.shape)
    print("Ctrl:", datactrl.shape)
    SX,SY,SZ = dataref.shape

    # Get input coords
    #X,Y,Z = np.mgrid[:SX, :SY, :SZ]
    #coords_in = np.c_[X.flatten(), Y.flatten(), Z.flatten()]

    # Transform
    #coords_out = main.ndi.affine_transform(coords_in, mat_affine[:3,:3])
    affine_scale_rotation = mat_affine[:3,:3]
    shift = mat_affine[:-1, -1]
    shift[0] = aff_z[0, -1]
    print("Affine:")
    print(affine_scale_rotation)
    print("Shift:")
    print(shift)
    # Get Scale for debugging
    scales = [
        np.linalg.norm(mat_affine[:-1, i])
        for i in range(3)
    ]
    scales[0] = aff_z[0,0]
    print("SCALE:", scales)
    #FIXME:Try scaling shift?
    print("SCALED SHIFT:", np.array(scales)*shift)
    affine_scale_rotation[0,0] = aff_z[0,0]



    dataref2 = main.ndi.affine_transform(dataref, affine_scale_rotation,
        offset=shift)
    datactrl2 = main.ndi.affine_transform(datactrl, affine_scale_rotation,
        offset=shift)

    # Now show max projections
    if False:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.imshow(dataref.max(axis=0))
        ax.set_title("Reference In")
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.imshow(dataref2.max(axis=0))
        ax.set_title("Reference transformed")
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.imshow(datactrl.max(axis=0))
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.imshow(datactrl2.max(axis=0))
        ax.set_title("Ctrl transformed")
        plt.show()

    # Can we get good agreement using just the 2d result at least??
    dataref2 = main.ndi.affine_transform(dataref.max(axis=0),
        matt2d[:2,:2],
        offset=matt2d[:-1,-1],
        output_shape=datactrl.shape[1:])
    print("Loading label data...")
    sys.stdout.flush()
    lbl = load_label_data()
    #lbl = load_label_data_overlapping()
    print("LOADED LABEL DATA", lbl.shape)
    sys.stdout.flush()
    lbl2 = main.ndi.affine_transform(lbl.max(axis=0),
        matt2d[:2,:2],
        offset=matt2d[:-1,-1],
        output_shape=datactrl.shape[1:])

    # Now show max projections
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(lbl.max(axis=0))
    ax.set_title("Label in")
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(dataref.max(axis=0))
    ax.set_title("Reference In")
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(dataref2)
    ax.set_title("Reference transformed")
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(datactrl.max(axis=0))
    ax.set_title("Ctrl in")
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(datactrl.max(axis=0), cmap='gray')
    ax.imshow(dataref2, alpha=0.3)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(datactrl.max(axis=0), cmap='gray')
    ax.imshow(lbl2, alpha=0.3)
    plt.show()

def test_count_overlaps_labels(
        labelfile = "/home/jeremy/Downloads/MaskDatabase.mat",
        ):
    print("Loading label data from Matlab file...(using h5py)")
    sys.stdout.flush()
    import h5py
    f = h5py.File(labelfile)
    W = int(f['width'].value[0, 0])
    H = int(f['height'].value[0, 0])
    Z = int(f['Zs'].value[0, 0])

    # Extract the sparse matrix components
    outlines = f['MaskDatabaseOutlines']
    data = outlines['data'].value.copy()
    ir = outlines['ir'].value.copy()
    jc = outlines['jc'].value.copy()

    # Try reconstituting
    label_flat = np.zeros(W*H*Z, dtype='int32')
    #label_flat[ir] = data
    # Options here for order are C, F, or A
    label = np.reshape(label_flat, (Z,W,H), order="C")


    if True:
        print("Iterating over %d labels..."%len(jc))
        sys.stdout.flush()
        for i in range(len(jc)-1):
            label_flat = np.zeros(W*H*Z, dtype='int32')
            label_flat[ir[jc[i]:jc[i+1]]] = data[jc[i]:jc[i+1]]
            # Options here for order are C, F, or A
            label_now = np.reshape(label_flat, (Z,W,H), order="C")
            # FILL
            label_now = main.ndi.label(label_now>0)[0]
            label[label_now>0] += 1
            sys.stdout.write("x")
            sys.stdout.flush()
    label = label.transpose([0,2,1])
    print("MAX  OVERLAP:")
    print(label.max())

def convert_label_data(
        labelfile = "/home/jeremy/Downloads/MaskDatabase.mat",
        ):
    #lbl = load_label_data_overlapping()
    #print("Label:", lbl.shape, lbl.min(), lbl.max())
    #main.skio.imsave(os.path.join(os.path.dirname(labelfile), "MaskDatabase.tif"),
    #    lbl)
    print("Loading label data from Matlab file...(using h5py)")
    sys.stdout.flush()
    import h5py
    f = h5py.File(labelfile)
    W = int(f['width'].value[0, 0])
    H = int(f['height'].value[0, 0])
    Z = int(f['Zs'].value[0, 0])

    # Extract the sparse matrix components
    outlines = f['MaskDatabaseOutlines']
    data = outlines['data'].value.copy()
    ir = outlines['ir'].value.copy()
    jc = outlines['jc'].value.copy()

    ## Try reconstituting
    #label_flat = np.zeros(W*H*Z, dtype='int32')
    #label_flat[ir] = data
    ## Options here for order are C, F, or A
    #label = np.reshape(label_flat, (Z,W,H), order="C")
    #dirname = os.path.dirname(labelfile)
    outdir = os.path.join("%s_labels"%labelfile)
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    print("Iterating over %d labels..."%len(jc))
    sys.stdout.flush()
    for i in range(len(jc)-1):
        label_flat = np.zeros(W*H*Z, dtype='int32')
        label_flat[ir[jc[i]:jc[i+1]]] = data[jc[i]:jc[i+1]]
        # Options here for order are C, F, or A
        label_now = np.reshape(label_flat, (Z,W,H), order="C")
        #label_now = main.ndi.label(label_now>0)[0]
        label_now = main.ndi.binary_fill_holes(label_now>0)
        # Save to compressed image
        main.skio.imsave(
            os.path.join(outdir, "%0.6d.tif"%i),
            (label_now>0).astype("uint8"),
            compress=9,
        )

        sys.stdout.write("x")
        sys.stdout.flush()

    #label = label.transpose([0,2,1])
    #return label

def output_label_names(
        labelfile = "/home/jeremy/Downloads/MaskDatabase.mat",
        ):
    import h5py
    f = h5py.File(labelfile)
    W = int(f['width'].value[0, 0])
    H = int(f['height'].value[0, 0])
    Z = int(f['Zs'].value[0, 0])

    # Extract the sparse matrix components
    names = []
    for ref in f['MaskDatabaseNames']:
        name = f[ref[0]].value
        names.append("".join((chr(n) for n in name)))
    outlines = f['MaskDatabaseOutlines']
    data = outlines['data'].value
    ir = outlines['ir'].value
    jc = outlines['jc'].value

    # now assuming that jc indexes into ir
    for i in range(len(jc)-1):
        name = names[i] if (i < len(names)) else "DUNNO"
        print(name)

##=========================================================
## Main script entry
##=========================================================

if __name__ == '__main__':
    print("Running testing")
    #test_registration()
    #profile_registration()
    #test_plotting()
    #test_batching()
    #test_loading_label_data()
    #test_register_against_label_data(do_reshape=True)
    #test_register_against_ref_data()
    #convert_label_data()
    #output_label_names()
    #test_count_overlaps_labels()

    test_load_roi_set()

    #convert_rois(
    #    path= "".join([
    #        "/data/new_data_root/dw388/201607/ROI Info",
    #        #"/ZF Brain Reference.tif_ROIS"
    #        #"/Control Fish 1 Strych.tif_ROIS"
    #        "/200uM Fish 1 Strych.tif_ROIS"
    #    ])
    #)
