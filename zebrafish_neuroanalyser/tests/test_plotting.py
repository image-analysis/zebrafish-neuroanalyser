#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Test plotting functions
#
import unittest
from unittest import mock
import zebrafish_neuroanalyser.plotting as plotting
import numpy as np
from zebrafish_neuroanalyser.tests.utility import random_string
import matplotlib.pyplot as plt
import os


def circleim(radius, size, width=5):
    width0sq = (radius-width/2)**2
    width1sq = (radius+width/2)**2
    X,Y = np.meshgrid(*(range(s) for s in size))
    distimsq = (X-size[0])**2 + (Y-size[0])**2
    im = distimsq > width0sq
    im = im & (distimsq < width1sq)
    return np.array([im,im])

class Test_plot_rois_on_max_proj(unittest.TestCase):

    def setUp(self):
        self.imsize = (400,400)
        self.im_max = 10+5*np.random.rand(*self.imsize)
        self.allrois = {
            i : circleim(i+20, self.imsize)
            for i in range(20)
        }
        self.output_path = random_string(50)

    @mock.patch("matplotlib.pyplot.Figure.savefig")
    def test_basic_plot(self,
            mocksavefig,
            ):
        plotting.plot_rois_on_max_proj(
            self.im_max,
            self.allrois,
            self.output_path
        )
        mocksavefig.assert_called_with(
            os.path.join(self.output_path,"labelled_regions.png")
        )


class Test_output_images_and_excel_sheets_for_time_projected_data(unittest.TestCase):

    def setUp(self):
        self.imsize = (400,400)
        self.ims = [ np.random.rand(*self.imsize) for i in range(10) ]
        self.name = random_string(50)
        self.output_path  = random_string(50)
        self.output_types = ["png"]
        self.tics_min = 5
        self.tics_maj = 20
        self.tic_maj_font = 8
        self.tic_min_font = 6
        self.archive=""

    @mock.patch("openpyxl.Workbook.save")
    @mock.patch("matplotlib.pyplot.Figure.savefig")
    @mock.patch("skimage.io.imsave")
    def test_basic_call(self,
            mockimsave,
            mocksavefig,
            mocksavewb,
            ):
        plotting.output_images_and_excel_sheets_for_time_projected_data(
            self.ims,
            self.name,
            self.output_path,
            self.output_types,
            tics_min = self.tics_min,
            tics_maj = self.tics_maj,
            tic_maj_font = self.tic_maj_font,
            tic_min_font = self.tic_min_font,
            archive = self.archive,
        )
        mocksavewb.assert_called_with(
            os.path.join(self.output_path, "%s_data.xlsx"%self.name)
        )
        i = len(self.ims)-1
        ext= self.output_types[-1]
        mocksavefig.assert_called_with(
            os.path.join(self.output_path, "%s_z_%d.%s"%(self.name,i,ext))
        )


class TestPlotLines(unittest.TestCase):
    def setUp(self):
        self.N = 10000
        self.T = 100
        self.lines = np.random.rand(self.N, self.T)

    def test_plot_lines(self):
        plotting.plot_lines(self.lines)
