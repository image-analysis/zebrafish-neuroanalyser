#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Test IO functions
#

import unittest
from unittest import mock
import zebrafish_neuroanalyser.dataio as zio
from zebrafish_neuroanalyser.tests.utility import do_nothing, random_string
import logging
import zipfile
import numpy as np
import builtins
import sys
import io
import json
import os
import random

class Test_load_and_resample(unittest.TestCase):
    def setUp(self):
        # Setup function before each test method
        # Generate test data in-memory (use mock module)
        self.filenames = ["file1", "file2"]
        self.sample_factor=[3,3]
        self.shape_in = (10,99,99)
        self.t_in = len(self.filenames)
        # Over-ride skimage.io.imread and tifffile.imread

    @mock.patch('skimage.io.imread')
    @mock.patch('skimage.external.tifffile.imread')
    def test_load_and_resample_sample_factor(
            self,
            mockskimread,
            mocktiffimread,
            ):
        # Create some random data
        randomdata = np.random.rand(*self.shape_in)
        mockskimread.return_value = randomdata
        mocktiffimread.return_value = randomdata

        data, shape0 = zio.load_and_resample(
            self.filenames,
            sample_factor=self.sample_factor,
            update_function=do_nothing,
            align_on_load=False,
            TMIN=0,
            TMAX=0,
            return_shape0=True,
            save_resampled=False,
            load_resampled=False,
        )
        # Check the shapes
        self.assertEqual(shape0, (self.t_in,)+ self.shape_in)
        self.assertEqual(data.shape, (self.t_in, self.shape_in[0])+ tuple(
                s//fact
                for s,fact in zip(self.shape_in[1:],self.sample_factor)
            )
        )
        # Could now check the means...

    @mock.patch('skimage.io.imread')
    @mock.patch('skimage.external.tifffile.imread')
    @mock.patch('zebrafish_neuroanalyser.utility.default_update_function')
    @mock.patch('builtins.print')
    def test_load_and_resample_update_function_none(
            self,
            mockprint,
            mockupdatefunc,
            mocktiffimread,
            mockskimread,
            ):
        # Create some random data
        randomdata = np.random.rand(*self.shape_in)
        mockskimread.return_value = randomdata
        mocktiffimread.return_value = randomdata

        data, shape0 = zio.load_and_resample(
            self.filenames,
            sample_factor=self.sample_factor,
            update_function=None,
            align_on_load=False,
            TMIN=0,
            TMAX=0,
            return_shape0=True,
            save_resampled=False,
            load_resampled=False,
        )
        # Check the shapes
        self.assertEqual(shape0, (self.t_in,)+ self.shape_in)
        self.assertEqual(data.shape, (self.t_in, self.shape_in[0])+ tuple(
                s//fact
                for s,fact in zip(self.shape_in[1:],self.sample_factor)
            )
        )
        # Could now check the means...


    @mock.patch('skimage.io.imread')
    @mock.patch('skimage.external.tifffile.imread')
    def test_load_and_resample_single_file(
            self,
            mockskimread,
            mocktiffimread,
            ):
        # Create some random data
        randomdata = np.random.rand(2, *self.shape_in)
        mockskimread.return_value = randomdata
        mocktiffimread.return_value = randomdata

        data, shape0 = zio.load_and_resample(
            self.filenames[0],
            sample_factor=self.sample_factor,
            update_function=do_nothing,
            align_on_load=False,
            TMIN=0,
            TMAX=0,
            return_shape0=True,
            save_resampled=False,
            load_resampled=False,
        )
        # Check the shapes
        self.assertEqual(shape0, (self.t_in,)+ self.shape_in)
        self.assertEqual(data.shape, (self.t_in, self.shape_in[0])+ tuple(
                s//fact
                for s,fact in zip(self.shape_in[1:],self.sample_factor)
            )
        )
        # Could now check the means...

class Test_load_resampled_data(unittest.TestCase):
    def setUp(self):
        self.filenames = ["filename1", "filename2"]
        self.data0_shape = [42,42]
        self.kwargs = {
            "data0_shape": self.data0_shape,
            "filenames":self.filenames,
        }
        self.different_kwargs = {"dummykey":42}
        self.different_kwargs.update(self.kwargs)
        self.metadata_file = io.StringIO()
        json.dump(self.kwargs, self.metadata_file)
        self.metadata_file.seek(0)

        self.empty_metadata_file = io.StringIO()
        json.dump({}, self.empty_metadata_file)
        self.empty_metadata_file.seek(0)

        self.outfolder = "%s_intermediate"%self.filenames[0]
        self.metadata_filename = os.path.join(
            self.outfolder,
            "load_parameters.json")
        self.resampled_filename = os.path.join(self.outfolder, "resampled_data.tif")

    @mock.patch('os.path.isdir')
    @mock.patch('os.path.isfile')
    @mock.patch('builtins.open')
    def test_no_output_folder(
            self,
            mockopen,
            mockisfile,
            mockisdir,
            ):
        mockisdir.side_effect = lambda d: d!=self.outfolder
        mockisfile.return_value = True
        mockopen.return_value = self.metadata_file
        with self.assertLogs(zio.logger, level="DEBUG") as cm:
            ret = zio.load_resampled_data(
                **self.kwargs
            )
        self.assertEqual(ret, (None, None))
        self.assertTrue(any(["Intermediate folder does not exist" in message
            for message in cm.output]))

    @mock.patch('os.path.isdir')
    @mock.patch('os.path.isfile')
    @mock.patch('builtins.open')
    def test_no_metadata_file(
            self,
            mockopen,
            mockisfile,
            mockisdir,
            ):
        mockisfile.side_effect = lambda d: d!=self.metadata_filename
        mockisdir.return_value = True
        mockopen.return_value = self.metadata_file
        with self.assertLogs(zio.logger, level="DEBUG") as cm:
            ret = zio.load_resampled_data(
                **self.kwargs
            )
        self.assertEqual(ret, (None, None))
        self.assertTrue(any(["Metadata file does not exist" in message
            for message in cm.output]))


    @mock.patch('os.path.isdir')
    @mock.patch('os.path.isfile')
    @mock.patch('builtins.open')
    def test_no_resampled_data_file(
            self,
            mockopen,
            mockisfile,
            mockisdir,
            ):
        mockisfile.side_effect = lambda d: d!=self.resampled_filename
        mockisdir.return_value = True
        mockopen.return_value = self.metadata_file
        with self.assertLogs(zio.logger, level="DEBUG") as cm:
            ret = zio.load_resampled_data(
                **self.kwargs
            )
        self.assertEqual(ret, (None, None))
        self.assertTrue(any(["Resampled data file does not exist" in message
            for message in cm.output]))


    @mock.patch('os.path.isdir')
    @mock.patch('os.path.isfile')
    @mock.patch('builtins.open')
    def test_wrong_metadata(
            self,
            mockopen,
            mockisfile,
            mockisdir,
            ):
        mockisfile.return_value = True
        mockisdir.return_value = True
        mockopen.return_value = self.empty_metadata_file
        with self.assertLogs(zio.logger, level="DEBUG") as cm:
            ret = zio.load_resampled_data(
                **self.kwargs
            )
        self.assertEqual(ret, (None, None))
        self.assertTrue(any(["Metadata data0_shape not set" in message
            for message in cm.output]))


    @mock.patch('os.path.isdir')
    @mock.patch('os.path.isfile')
    @mock.patch('builtins.open')
    def test_wrong_kwargs(
            self,
            mockopen,
            mockisfile,
            mockisdir,
            ):
        mockisfile.return_value = True
        mockisdir.return_value = True
        mockopen.return_value = self.metadata_file
        with self.assertLogs(zio.logger, level="DEBUG") as cm:
            ret = zio.load_resampled_data(
                **self.different_kwargs
            )
        self.assertEqual(ret, (None, None))
        self.assertTrue(any(["Metadata not the same" in message
            for message in cm.output]))


    @mock.patch('skimage.io.imread')
    @mock.patch('os.path.isdir')
    @mock.patch('os.path.isfile')
    @mock.patch('builtins.open')
    def test_load(
            self,
            mockopen,
            mockisfile,
            mockisdir,
            mockimread,
            ):
        mockisfile.return_value = True
        mockisdir.return_value = True
        mockopen.return_value = self.metadata_file
        mockimread.return_value = 42
        ret = zio.load_resampled_data(
            **self.kwargs
        )
        self.assertEqual(ret, (42, self.data0_shape))


class Test_save_resampled_data(unittest.TestCase):
    def setUp(self):
        self.data = 42
        self.filenames = ["filename1", "filename2"]
        self.data0_shape = [42,42]
        self.kwargs = {
            "data0_shape": self.data0_shape,
            "filenames":self.filenames,
        }
        self.metadata_file = io.StringIO()

        self.outfolder = "%s_intermediate"%self.filenames[0]
        self.metadata_filename = os.path.join(
            self.outfolder,
            "load_parameters.json")
        self.resampled_filename = os.path.join(self.outfolder, "resampled_data.tif")

    @mock.patch('skimage.io.imsave')
    @mock.patch('os.makedirs')
    @mock.patch('os.path.isdir')
    @mock.patch('os.path.isfile')
    @mock.patch('builtins.open')
    def test_no_output_folder(
            self,
            mockopen,
            mockisfile,
            mockisdir,
            mockmakedirs,
            mockimsave,
            ):
        mockisdir.return_value = False
        mockisfile.return_value = True
        mockopen.return_value = self.metadata_file
        ret = zio.save_resampled_data(
            self.data,
            **self.kwargs
        )
        # Make sure we tried to create the output folder
        mockmakedirs.assert_called_with(self.outfolder)
        mockimsave.assert_called_with(
            self.resampled_filename,
            self.data,
        )


class Test_skioimsave(unittest.TestCase):
    def setUp(self):
        self.args = [42, "We are the knights of Ni!"]
    @mock.patch('skimage.io.imsave')
    def test_imsave( self, mockimsave ):
        ret = zio.skioimsave(*self.args)
        mockimsave.assert_called_with(*self.args)


class Test_put_in_archive(unittest.TestCase):

    def setUp(self):
        self.files = [random_string(20)]
        self.archive = random_string(10)

    @mock.patch('os.remove')
    @mock.patch('zipfile.ZipFile')
    def test_one_file( self, mockzip, mockremove):
        mockzip.return_value = mock.MagicMock(spec=io.StringIO)
        ret = zio.put_in_archive(self.files[0], self.archive)
        mockzip.assert_called_with(
            self.archive,
            "a",
            compression=zipfile.ZIP_DEFLATED
        )
        mockremove.assert_called_with( self.files[0] )

    @mock.patch('os.remove')
    @mock.patch('zipfile.ZipFile')
    def test_fail( self, mockzip, mockremove):
        mockzip.return_value = mock.MagicMock(spec=io.StringIO)
        mockremove.side_effect = Exception("Dummy")
        with self.assertLogs(zio.logger, level="DEBUG") as cm:
            ret = zio.put_in_archive(self.files[0], self.archive)
        self.assertTrue(any([
            "Unable to place %s into output archive because:" % self.files[0]
            in message for message in cm.output]))


class Test_load_points_from_file(unittest.TestCase):
    def setUp(self):
        self.filename = random_string(20)
        self.npoints = 50
        self.points = [[random.randint(0,1000), random.randint(0,1000)]
            for i in range(self.npoints)]
        self.pointsfile = io.StringIO()
        for p in self.points:
            self.pointsfile.write("blah,%d,%d\n"%tuple(p))
        self.pointsfile.seek(0)

    @mock.patch('builtins.open')
    def test_simple( self, mockopen):
        mockopen.return_value = self.pointsfile
        ret = zio.load_points_from_file(self.filename)
        mockopen.assert_called_with(self.filename)
        self.assertTrue(np.array_equal(ret, np.array(self.points)))

    @mock.patch('builtins.open')
    def test_shuffle( self, mockopen):
        mockopen.return_value = self.pointsfile
        ret = zio.load_points_from_file(self.filename, shuffle=[1,0])
        mockopen.assert_called_with(self.filename)
        self.assertTrue(np.array_equal(ret, np.array(self.points)[:,[1,0]]))


class Test_get_outfolder_and_filenames_for_saving_rois(unittest.TestCase):
    def setUp(self):
        self.filenames = ["ABC", "DEF"]
        self.outfolder = "%s_intermediate"%(self.filenames[0])

    @mock.patch('os.path.isdir')
    @mock.patch('os.makedirs')
    def test_simple_folder_exist(self, makedirs, isdir):
        isdir.return_value = True
        zio.get_outfolder_and_filenames_for_saving_rois(self.filenames)
        isdir.assert_called_with(self.outfolder)
        self.assertFalse(makedirs.called)

    @mock.patch('os.path.isdir')
    @mock.patch('os.makedirs')
    def test_simple_folder_not_exist(self, makedirs, isdir):
        isdir.return_value = False
        zio.get_outfolder_and_filenames_for_saving_rois(self.filenames)
        isdir.assert_called_with(self.outfolder)
        makedirs.assert_called_with(self.outfolder)


class Test_write_rois_to_zip_archive(unittest.TestCase):
    def setUp(self):
        self.filename = "testarch.zip"
        self.roifilebase = "foo%d"
        self.selected = [1,2,3]
        self.allrois = {1: np.array([1]), 2: np.array([2]), 3: np.array([3]), 4:np.array([4])}

    @mock.patch("zipfile.ZipFile")
    @mock.patch("skimage.external.tifffile.imsave")
    def test_simple_write(self, imsave, zfile):
        zf = zfile.return_value
        zio.write_rois_to_zip_archive(self.filename, self.roifilebase, self.selected, self.allrois)
        zfile.assert_called_with(self.filename, "w", compression=zipfile.ZIP_DEFLATED)
        self.assertEqual(imsave.call_count, len(self.selected))
        for call_args, num in zip(imsave.call_args_list, self.selected):
            expect = self.allrois[num].astype('uint8')
            np.testing.assert_array_equal(expect, call_args[0][1])

class Test_save_aligned_rois_to_file(unittest.TestCase):
    def setUp(self):
        # create the inputs
        self.allrois = {}
        self.roinames = []
        self.filenames = ["ABC", "DEF"]

    def test_basic_usage(self):
        """
        Test the basic call
        """
        # Check that the first filename is used to create the
        # output folder and corresponding metadata_file and roinames_file
        pass
    """
        allrois,
        roinames,
        filenames=[],
        **kwargs
        ):


    outfolder = "%s_intermediate"%filenames[0]
    metadata_file = os.path.join(outfolder, "roi_parameters.json")
    roinames_file = os.path.join(outfolder, "roi_names.json")
    roi_file = os.path.join(outfolder, "aligned_rois.zip")
    if not os.path.isdir(outfolder):
        os.makedirs(outfolder)
    with open(metadata_file, "w") as fout:
        kwargs["filenames"] = filenames
        for k, v in kwargs.items():
            if isinstance(v, np.ndarray):
                kwargs[k] = v.tolist()
        json.dump(kwargs, fout)
    with open(roinames_file, "w") as fout:
        json.dump(roinames, fout)
    roiarchive = zipfile.ZipFile(roi_file, "w", compression=zipfile.ZIP_DEFLATED)
    roifilebase = kwargs["roifilebase"]
    for num in kwargs["selected"]:
        name = roifilebase%num
        fileobj = io.BytesIO()
        tifffile.imsave(fileobj, allrois[num].astype('uint8'))
        fileobj.seek(0)
        roiarchive.writestr(name, fileobj.read())
    """
