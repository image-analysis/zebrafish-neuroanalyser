#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Test Batch GUI functions
#

import unittest
from unittest import mock
import zebrafish_neuroanalyser.gui.batch as gui

app = gui.QtWidgets.QApplication([])

class Test_batch_gui(unittest.TestCase):
    def test_batch_gui(self):
        # Check that we create the correct object
        dialog = gui.BatchDialog()
        self.assertTrue(isinstance(dialog, gui.BatchDialog))
