#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION :
#

import unittest
from unittest import mock
import zebrafish_neuroanalyser.identify_clusters as identify_clusters
import numpy as np
import builtins
import io

class Test_analyze_label_cluster_relationships(unittest.TestCase):
    def setUp(self):
        self.n_clusters = 10
        self.n_regions = 20
        self.size = (10, 50, 50)
        # Clusters is a labelled array
        self.random_clusters = np.random.randint(0, self.n_clusters, self.size)
        # Regions can overlap - is a dict of binary arrays
        self.random_regions = {
            i: np.random.rand(*self.size) > 0.8
            for i in range(self.n_regions)
        }

    def test_random(self):
        stats = identify_clusters.analyze_label_cluster_relationships(
            self.random_regions,
            self.random_clusters,
        )
        self.assertEqual(len(stats), 2)
        self.assertEqual(
            sorted(stats.keys()),
            ["clustercounts", "regioncounts"]
        )

class Test_analyze_label_cluster_relationships_and_save(unittest.TestCase):
    """
    Just need to make sure the correct call is made to analyze_label_cluster_relationships
    and that the output of that is properly parsed
    """
    def setUp(self):
        self.filename = "foo.csv"
        self.regions = "spam"
        self.clusters = "eggs"
        self.region_counts_empty_data = {1 : {}, 2 : {}, 3: {}}
        self.clust_counts_empty_data = {1 : {}, 2 : {}, 3: {}}
        self.region_counts_empty = {}
        self.clust_counts_empty = {}
        #dummy_val = {"region_ids" : np.
        self.region_counts_empty_data = {1 : {}, 2 : {}, 3: {}}
        self.clust_counts_empty_data = {1 : {}, 2 : {}, 3: {}}

    @mock.patch("zebrafish_neuroanalyser.identify_clusters.analyze_label_cluster_relationships")
    @mock.patch('builtins.open')
    def test_no_regions(self, mockopen, mockanalyze):
        # Use an io.StringIO
        mockopen.return_value = io.StringIO()
        mockanalyze.return_value = {'regioncounts': self.region_counts_empty, 'clustercounts': self.clust_counts_empty}
        identify_clusters.analyze_label_cluster_relationships_and_save(
            self.regions,
            self.clusters,
            self.filename,
        )

    @mock.patch("zebrafish_neuroanalyser.identify_clusters.analyze_label_cluster_relationships")
    @mock.patch('builtins.open')
    def test_empty_data(self, mockopen, mockanalyze):
        # Use an io.StringIO
        mockopen.return_value = io.StringIO()
        mockanalyze.return_value = {'regioncounts': self.region_counts_empty_data, 'clustercounts': self.clust_counts_empty_data}
        # Now we should get a key error when trying to access a dictionary item that doesn't exist
        self.assertRaises(KeyError,
            identify_clusters.analyze_label_cluster_relationships_and_save,
                self.regions,
                self.clusters,
                self.filename,
        )
    """
    with open(filename, 'w') as fout:
        wrt = csv.writer(fout)
        wrt.writerow(["Per cluster analysis",])
        for cid, data in sorted(rels['clustercounts'].items()):
            wrt.writerow(["Cluster ID", str(cid)])
            wrt.writerow(["Regions",] + list(data['region_ids']))
            wrt.writerow(["Areas (px)",] +list(data['counts']))
            wrt.writerow(["Fractional Area (of whole cluster area)",] + list(data['counts']/np.sum(data['counts'])))
        wrt.writerow([])
        wrt.writerow(["Per labelled region analysis",])
        for rid, data in sorted(rels['regioncounts'].items()):
            wrt.writerow(["Region ID", str(rid)])
            wrt.writerow(["Clusters",] + list(data['cluster_ids']))
            wrt.writerow(["Areas (px)",] +list(data['counts']))
            wrt.writerow(["Fractional Area (of whole region area)",] + list(data['counts']/np.sum(data['counts'])))
    """
