#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Test the processing functions
#

import unittest
from unittest import mock
import zebrafish_neuroanalyser.processing as processing
from zebrafish_neuroanalyser.tests.utility import random_string, do_nothing
import numpy as np
import io
import os

class Test_normalize_data(unittest.TestCase):
    def setUp(self):
        self.tmax  = 100
        self.shape = (self.tmax,10,50,50)
        self.data = np.random.rand(*self.shape)

    def test_high_baseline(self):
        res = processing.normalize_data(self.data+10)
        # Check that normalization has happened
        # We basically just subtract and divide by the baseline
        # So the mean and std should both decrease for baseline > 1
        self.assertLess(res.mean(), self.data.mean())
        self.assertLess(res.std(), self.data.std())

    def test_low_baseline(self):
        # Data should
        res = processing.normalize_data(self.data+0.1)
        # Check that normalization has happened
        # We basically just subtract and divide by the baseline
        # So the mean and std should both increase for baseline < 1
        self.assertGreater(res.mean(), self.data.mean())
        self.assertGreater(res.std(), self.data.std())



class Test_savitzky_golay(unittest.TestCase):
    def setUp(self):
        self.tmax  = 100
        self.shape = (self.tmax,)
        self.data = np.random.rand(*self.shape)

    def test_basic(self):
        res = processing.savitzky_golay(
            self.data+10,
            window_size=11,
            order=2,
            )
        # Check that normalization has happened
        # We basically just subtract and divide by the baseline
        # So the mean and std should both decrease for baseline > 1
        #import matplotlib.pyplot as plt
        #plt.plot(res)
        #plt.show()
        #self.assertLess(res.mean(), self.data.mean())
        #self.assertLess(res.std(), self.data.std())
        # TODO: Check something here
    def test_low_baseline(self):
        # Data should
        res = processing.normalize_data(self.data+0.1)
        # Check that normalization has happened
        # We basically just subtract and divide by the baseline
        # So the mean and std should both increase for baseline < 1
        self.assertGreater(res.mean(), self.data.mean())
        self.assertGreater(res.std(), self.data.std())
