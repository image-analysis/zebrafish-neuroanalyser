#
# FILE        : identify_clusters.py
# CREATED     : 14/10/16 07:47:50
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Load the cluster id data and labelled region data
#               and identify which labels a cluster is composed of
#               and which clusters a label holds
#
import os
import numpy as np
import csv
def analyze_label_cluster_relationships(regions, clusters):
    """
    Compare the two arrays;
    return details of which clusters are contained in each labelled region
    and vice-versa
    """
    # iterate over each region and get the cluster membership
    region_counts = {}
    clust_counts = {clusid : {'region_ids':[], 'counts':[]} for clusid in np.unique(clusters)}
    for name, region in regions.items():
        clustids = clusters[region]
        # Get the counts of the clusters
        counts = np.bincount(clustids)
        idsuniq = np.arange(len(counts))
        idsuniq = idsuniq[counts>0]
        counts = counts[counts>0]
        region_counts[name] = {
            'cluster_ids' : idsuniq,
            'counts'      : counts}
        for cid, num in zip(idsuniq, counts):
            clust_counts[cid]['region_ids'].append(name)
            clust_counts[cid]['counts'].append(num)

    # Lets see it the other way around
    for cid, data in clust_counts.items():
        #print("Cluster ID", cid)
        #print("regions  :", data['region_ids'])
        #print("areas    :", data['counts'])
        #print("fraction :", data['counts']/np.sum(data['counts']))
        pass
    return {'regioncounts': region_counts, 'clustercounts':clust_counts}

def analyze_label_cluster_relationships_and_save(regions, clusters, filename):
    rels = analyze_label_cluster_relationships(regions, clusters)

    with open(filename, 'w') as fout:
        wrt = csv.writer(fout)
        wrt.writerow(["Per cluster analysis",])
        for cid, data in sorted(rels['clustercounts'].items()):
            wrt.writerow(["Cluster ID", str(cid)])
            wrt.writerow(["Regions",] + list(data['region_ids']))
            wrt.writerow(["Areas (px)",] +list(data['counts']))
            wrt.writerow(["Fractional Area (of whole cluster area)",] + list(data['counts']/np.sum(data['counts'])))
        wrt.writerow([])
        wrt.writerow(["Per labelled region analysis",])
        for rid, data in sorted(rels['regioncounts'].items()):
            wrt.writerow(["Region ID", str(rid)])
            wrt.writerow(["Clusters",] + list(data['cluster_ids']))
            wrt.writerow(["Areas (px)",] +list(data['counts']))
            wrt.writerow(["Fractional Area (of whole region area)",] + list(data['counts']/np.sum(data['counts'])))

