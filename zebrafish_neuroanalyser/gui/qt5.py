#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : GUI for launching main processing
#

# =========================================================
# Graphical library usage - try PyQt4 first - should be installed
# =========================================================

from PyQt5 import QtGui, QtCore, QtWidgets
import sys
import os
import tempfile
import time
import subprocess

from zebrafish_neuroanalyser.utility import (
    sort_filenames,
    trunc,
    logger,
)

# =========================================================
# Main GUI classes
# =========================================================


class Worker(QtCore.QThread):
    completed_run = QtCore.pyqtSignal()
    update = QtCore.pyqtSignal(int, int, str)

    def __init__(self, main_func, *args, **kwargs):
        super(Worker, self).__init__(*args, **kwargs)
        self.main_func = main_func
        self.files = []
        self.output_path = None
        self.align_frames = None
        self.sample_factor = 3
        self.TMIN = 0
        self.TMAX = 0
        self.moviewriter = None
        self.selected_rois_file = None

    def run(self):

        def update_progress(n=-1, N=-1, message=""):
            self.update.emit(n, N, message)

        def show_error(err):
            QtWidgets.QMessageBox.critical(self,
                                           "Critical",
                                           err)

        self.main_func(
            filenames=self.files,
            update_function=update_progress,
            error_message_function=show_error,
            output_path=self.output_path,
            align_frames=self.align_frames,
            sample_factor=[self.sample_factor, self.sample_factor],
            TMIN=self.TMIN,
            TMAX=self.TMAX,
            moviewriter=self.moviewriter,
            selected_rois_file=self.selected_rois_file,
        )
        self.completed_run.emit()


class MainDialog(QtWidgets.QDialog):
    def __init__(self, main_func, output_path_folder="output"):
        super(MainDialog, self).__init__()

        # Create widgets and layouts
        self.layout = QtWidgets.QVBoxLayout()
        # Info
        self.label = QtWidgets.QLabel("")

        # Input section
        self.lblInputFiles = QtWidgets.QLabel("No data selected")
        self.btnSelectData = QtWidgets.QPushButton("Select")
        if QtGui.QIcon.hasThemeIcon("document-open"):
            self.btnSelectData.setIcon(QtGui.QIcon.fromTheme("document-open"))
        self.chkAlignFrames = QtWidgets.QCheckBox("Align frames")

        # Output section
        self.lblOutput = QtWidgets.QLabel("Output:")
        self.btnSelectOutput = QtWidgets.QPushButton("Change")
        if QtGui.QIcon.hasThemeIcon("folder"):
            self.btnSelectOutput.setIcon(QtGui.QIcon.fromTheme("folder"))
        self.lineOutputDisplay = QtWidgets.QLineEdit()

        # Progress
        self.pbar = QtWidgets.QProgressBar()
        # Log
        self.log = QtWidgets.QTextEdit()
        # Start/cancel
        self.btnStartCancel = QtWidgets.QPushButton("Select input files first")

        self.lblStartFrame = QtWidgets.QLabel('Start frame')
        self.edtStartFrame = QtWidgets.QSpinBox()
        self.lblEndFrame = QtWidgets.QLabel('End frame')
        self.edtEndFrame = QtWidgets.QSpinBox()
        self.lblSampleSize = QtWidgets.QLabel('Resample factor')
        self.edtSampleSize = QtWidgets.QSpinBox()

        self.lblAnalysisGridSize = QtWidgets.QLabel('Grid Size (pixels)')
        self.edtAnalysisGridSize = QtWidgets.QSpinBox()

        self.inputLayout = QtWidgets.QGridLayout()
        self.inputLayout.addWidget(self.lblInputFiles, 0, 0, 4, 0)
        self.inputLayout.addWidget(self.btnSelectData, 0, 5, 1, 1)
        self.inputLayout.addWidget(self.chkAlignFrames, 1, 5, 1, 1)
        self.inputLayout.addWidget(self.lblStartFrame, 2, 0, 1, 1)
        self.inputLayout.addWidget(self.edtStartFrame, 2, 1, 1, 1)
        self.inputLayout.addWidget(self.lblEndFrame, 2, 2, 1, 1)
        self.inputLayout.addWidget(self.edtEndFrame, 2, 3, 1, 1)

        self.inputLayout.addWidget(self.lblSampleSize, 2, 4, 1, 1)
        self.inputLayout.addWidget(self.edtSampleSize, 2, 5, 1, 1)

        self.analysisLayout = QtWidgets.QGridLayout()
        self.analysisLayout.addWidget(self.lblAnalysisGridSize, 0, 0)
        self.analysisLayout.addWidget(self.edtAnalysisGridSize, 0, 1)

        self.outputLayout = QtWidgets.QHBoxLayout()
        self.outputLayout.addWidget(self.lineOutputDisplay)
        self.outputLayout.addWidget(self.btnSelectOutput)

        self.grpOutput = QtWidgets.QGroupBox("Output")
        self.grpOutput.setLayout(self.outputLayout)
        self.grpAnalysis = QtWidgets.QGroupBox("Analysis")
        self.grpAnalysis.setLayout(self.analysisLayout)
        self.grpInput = QtWidgets.QGroupBox("Input")
        self.grpInput.setLayout(self.inputLayout)

        self.layout.addWidget(self.label)
        self.layout.addWidget(self.grpInput)
        self.layout.addWidget(self.grpAnalysis)
        self.layout.addWidget(self.grpOutput)
        self.layout.addWidget(self.pbar)
        self.layout.addWidget(self.log)
        self.layout.addWidget(self.btnStartCancel)

        # Initialize states
        self.btnStartCancel.setEnabled(False)
        self.chkAlignFrames.setChecked(True)

        self.lblStartFrame.setEnabled(False)
        self.lblEndFrame.setEnabled(False)
        self.edtStartFrame.setEnabled(False)
        self.edtEndFrame.setEnabled(False)
        self.edtStartFrame.setMinimum(0)
        self.edtStartFrame.setMaximum(1000)
        self.edtEndFrame.setMinimum(0)
        self.edtEndFrame.setMaximum(1000)

        self.edtSampleSize.setValue(3)
        self.edtAnalysisGridSize.setMinimum(1)
        self.edtAnalysisGridSize.setMaximum(512)
        self.edtAnalysisGridSize.setValue(32)
        self.pbar.hide()

        self.pbar.setMaximum(0)
        self.pbar.setMinimum(0)

        self.log.setReadOnly(True)
        self.lineOutputDisplay.setReadOnly(True)

        self.setLayout(self.layout)

        # Add in associated variables
        self.files = []
        self.output_path = ""
        self.output_path_folder = output_path_folder
        self.progress_time_started = None

        self.resize(480, 300)
        self.setWindowTitle("Data Processing")
        self.prog_base_message = "Processing image stack..."

        self.worker = Worker(main_func)
        # Connect up signals and slots
        self.connect_signals_slots()

    def connect_signals_slots(self):
        self.worker.finished.connect(self.terminated)
        self.worker.completed_run.connect(self.completed_run)
        self.worker.update.connect(self.progress)
        self.btnStartCancel.clicked.connect(self.startcancel)
        self.btnSelectData.clicked.connect(self.set_filenames)
        self.btnSelectOutput.clicked.connect(self.selectoutput)
        self.edtStartFrame.valueChanged.connect(self.edtEndFrame.setMinimum)
        self.edtEndFrame.valueChanged.connect(self.edtStartFrame.setMaximum)

    def selectoutput(self):
        self.output_path = str(QtWidgets.QFileDialog.getExistingDirectory(
            None,
            "Select output directory", self.output_path))
        self.lineOutputDisplay.setText(trunc(self.output_path))
        self.addLog("Output changed to : %s" % trunc(self.output_path))

    def set_filenames(self):
        dlg = QtWidgets.QFileDialog(self)
        dlg.setWindowTitle("Select data")
        dlg.setFileMode(QtWidgets.QFileDialog.ExistingFiles)
        if(dlg.exec_()):
            # self.files = sorted([str(f) for f in dlg.selectedFiles()])
            files = [str(f) for f in dlg.selectedFiles()]
            self.files = sort_filenames(files,
                                        time_regex=r"TL(\d+)")

            if len(self.files) == 0:
                self.lblInputFiles.setText("No input selected")
                self.addLog("No input files selected")
                self.lblStartFrame.setEnabled(False)
                self.lblEndFrame.setEnabled(False)
                self.edtStartFrame.setEnabled(False)
                self.edtEndFrame.setEnabled(False)
                return
            elif len(self.files) == 1:
                self.lblInputFiles.setText(os.path.basename(self.files[0]))
                self.addLog("One input file selected : %s" %
                            trunc(self.files[0]))
                self.lblStartFrame.setEnabled(True)
                self.lblEndFrame.setEnabled(True)
                self.edtStartFrame.setEnabled(True)
                self.edtEndFrame.setEnabled(True)
            else:
                self.lblInputFiles.setText(
                    "%d files selected" % len(self.files))
                self.addLog("%d input files selected:" % len(self.files))
                if len(self.files) < 5:
                    for f in self.files:
                        self.addLog("- %s" % trunc(f))
                else:
                    for f in self.files[:2]:
                        self.addLog("- %s" % trunc(f))
                    self.addLog("  :")
                    for f in self.files[-2:]:
                        self.addLog("- %s" % trunc(f))
                self.lblStartFrame.setEnabled(True)
                self.lblEndFrame.setEnabled(True)
                self.edtStartFrame.setEnabled(True)
                self.edtEndFrame.setEnabled(True)
                self.edtStartFrame.setMaximum(len(self.files)-1)
                self.edtEndFrame.setMaximum(len(self.files)-1)
                self.edtStartFrame.setValue(0)
                self.edtEndFrame.setValue(len(self.files)-1)

            infiledir = os.path.dirname(self.files[0])
            self.output_path = os.path.join(infiledir, self.output_path_folder)
            self.lineOutputDisplay.setText(trunc(self.output_path))
            if not self.btnStartCancel.isEnabled():
                self.btnStartCancel.setEnabled(True)
                self.btnStartCancel.setText("Start")

    def setValue(self, *args, **kwargs):
        self.pbar.setValue(*args, **kwargs)

    def setMaximum(self, *args, **kwargs):
        self.pbar.setMaximum(*args, **kwargs)

    def setMinimum(self, *args, **kwargs):
        self.pbar.setMinimum(*args, **kwargs)

    def setLabelText(self, *args, **kwargs):
        self.label.setText(*args, **kwargs)

    def addLog(self, message):
        self.log.append(message)

    def terminated(self):
        self.pbar.hide()
        self.setLabelText("ABORTED")
        self.addLog('Processing ABORTED (worker thread terminated)')
        self.btnStartCancel.setText("Start")

    def completed_run(self):
        self.setLabelText("Finished processing!")
        self.btnStartCancel.setText("Start")
        self.pbar.hide()
        self.addLog('Finished processing!')
        launch_file_explorer(self.worker.output_path, self)

    def progress(self, n=-1, N=-1, message=""):
        if n > -1:
            if self.progress_time_started is None:
                self.progress_time_started = time.time()
            if (n > 0) and (N > -1):
                t_taken = time.time() - self.progress_time_started
                t_left = ((float(N) / n) - 1)*t_taken
                self.pbar.setFormat("%d %% - %ds remaining" %
                                    (100*float(n)/N, t_left))
            self.setValue(n)
            if N > -1:
                self.setMaximum(N)
        else:
            self.setMaximum(0)
            self.setMinimum(0)
            # reset the timer
            self.progress_time_started = None

        if message:
            self.setLabelText("%s\n%s" % (self.prog_base_message, message))
            self.addLog(message)
            # And still output to console
            print(message)

    def startcancel(self):
        if self.worker.isRunning():
            self.worker.terminate()
        else:
            self.pbar.show()
            self.btnStartCancel.setText("Cancel")
            # worker.files = prog.files[
            #    prog.edtStartFrame.value() : prog.edtEndFrame.value()+1]
            self.worker.files = self.files
            self.worker.TMIN = self.edtStartFrame.value()
            self.worker.TMAX = self.edtEndFrame.value()
            self.worker.output_path = self.output_path
            self.worker.align_frames = self.chkAlignFrames.isChecked()
            self.worker.sample_factor = self.edtSampleSize.value()
            self.worker.start()

# =========================================================
# Additional support functions
# =========================================================


def launch_file_explorer(path, parent=None):
    # Try cross-platform file-explorer opening...
    # Courtesy of: http://stackoverflow.com/a/1795849/537098
    if sys.platform == 'win32':
        subprocess.Popen(['start', path], shell=True)
    elif sys.platform == 'darwin':
        subprocess.Popen(['open', path])
    else:
        try:
            subprocess.Popen(['xdg-open', path])
        except OSError:
            # er, think of something else to try
            # xdg-open *should* be supported by recent Gnome, KDE, Xfce
            QtWidgets.QMessageBox.critical(
                parent,
                "Oops",
                "Couldn't launch the file explorer, sorry!\n"
                + "Manually open %s in your favourite file manager" % path)


def get_existing_folder_gui_standalone(message="Select directory"):
    app = QtWidgets.QApplication([])
    rootpath = QtWidgets.QFileDialog.getExistingDirectory(
        None, message, "")
    return str(rootpath)


# -----------------------------
# Main entry point
# -----------------------------


def main_runner(
        main_func,
        filenames=[],
        dev=False,
        moviewriter=None,
        output_path_folder="output",
        selected_rois_file=None,
):
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle("plastique")

    prog = MainDialog(main_func, output_path_folder=output_path_folder)
    if dev:
        prog.files = ["dev", ]
        prog.lblInputFiles.setText("dev")
        prog.btnStartCancel.setEnabled(True)
        prog.edtSampleSize.setValue(1)
        prog.output_path = os.path.join(tempfile.gettempdir(),
                                        os.path.basename(__file__))
        prog.btnStartCancel.setText("Run test data")

    # Pass parameters to the worker
    prog.worker.moviewriter = moviewriter
    prog.worker.selected_rois_file = selected_rois_file
    prog.show()

    app.exec_()
    prog.worker.terminate()
