#
# FILE        : process_neuro_data_batcher.py
# CREATED     : 02/11/15 14:14:57
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Create a list of directories,
#               then run the process_neuro_data main function
#               on each one
#

import sys
import os
import glob
from PyQt5 import QtCore, QtGui, QtWidgets
import multiprocessing
from zebrafish_neuroanalyser import main, utility

##=========================================================
## Worker Thread, creates multiprocessing Pool
##=========================================================

class Worker(QtCore.QThread):
    update = QtCore.pyqtSignal(int, int, str)

    def __init__(self, *args, **kwargs):
        super(Worker,self).__init__(*args, **kwargs)
        self.num_cores = 0
        self.func = None

    def run(self):
        self.pool = multiprocessing.Pool(self.num_cores)
        N = len(self.arglists)
        print("STARTING POOL")
        print("With %d vals"%N)
        print("len of first :",len(self.arglists[0]))
        for i, _ in enumerate(self.pool.imap_unordered(
            self.func, self.arglists) ):
            self.emit(QtCore.SIGNAL('update(int, int, QString)'), i, N, "")
        self.emit(QtCore.SIGNAL('completed_batch()'))


#------------------------------
# Wrapping of mains for multiprocessing imap call
#------------------------------
def wrapped_main(argskwargs):
    main.main(*argskwargs[0], **argskwargs[1])

##=========================================================
## Batcher dialog
##=========================================================

class BatchDialog(QtWidgets.QDialog):
    def __init__(self):
        super(BatchDialog, self).__init__()

        # Create widgets and layouts
        self.layout = QtWidgets.QVBoxLayout()


        self.lblNumCores   = QtWidgets.QLabel("Number of workers")
        self.edtNumCores   = QtWidgets.QSpinBox()

        # Input section
        self.lblRootPath = QtWidgets.QLabel("No root folder selected")
        self.btnSelectData = QtWidgets.QPushButton("Select")
        if QtGui.QIcon.hasThemeIcon("document-open"):
            self.btnSelectData.setIcon(QtGui.QIcon.fromTheme("document-open"))
        self.chkAlignFrames = QtWidgets.QCheckBox("Align frames");

        # Output section
        self.lblOutput = QtWidgets.QLabel("Output folder")
        self.lineOutputDisplay = QtWidgets.QLineEdit()

        # Progress
        self.pbar_total = QtWidgets.QProgressBar()
        self.pbars = {}


        # Start/cancel
        self.btnStartCancel = QtWidgets.QPushButton("Select root folder first")


        self.lblStartFrame = QtWidgets.QLabel('Start frame')
        self.edtStartFrame = QtWidgets.QSpinBox()
        self.lblEndFrame = QtWidgets.QLabel('End frame')
        self.edtEndFrame = QtWidgets.QSpinBox()
        self.lblSampleSize = QtWidgets.QLabel('Resample factor')
        self.edtSampleSize = QtWidgets.QSpinBox()

        self.batchLayout = QtWidgets.QGridLayout()
        self.batchLayout.addWidget(self.lblRootPath, 0, 0, 1, 4)
        self.batchLayout.addWidget(self.btnSelectData, 0, 4, 1, 1)
        self.batchLayout.addWidget(self.lblNumCores, 1, 0, 1, 4)
        self.batchLayout.addWidget(self.edtNumCores, 1, 4, 1, 1)


        self.inputLayout = QtWidgets.QGridLayout()
        self.inputLayout.addWidget(self.chkAlignFrames, 1, 5, 1, 1)

        self.inputLayout.addWidget(self.lblStartFrame, 2, 0, 1, 1)
        self.inputLayout.addWidget(self.edtStartFrame, 2, 1, 1, 1)
        self.inputLayout.addWidget(self.lblEndFrame, 2, 2, 1, 1)
        self.inputLayout.addWidget(self.edtEndFrame, 2, 3, 1, 1)

        self.inputLayout.addWidget(self.lblSampleSize, 2, 4, 1, 1)
        self.inputLayout.addWidget(self.edtSampleSize, 2, 5, 1, 1)


        self.outputLayout = QtWidgets.QHBoxLayout()
        self.outputLayout.addWidget(self.lblOutput)
        self.outputLayout.addWidget(self.lineOutputDisplay)

        self.progressLayout = QtWidgets.QVBoxLayout()
        self.progressLayout.addWidget(QtWidgets.QLabel('Total'))
        self.progressLayout.addWidget(self.pbar_total)
        self.grpProgress = QtWidgets.QGroupBox("Progress")
        self.grpProgress.setLayout(self.progressLayout)
        self.grpOutput = QtWidgets.QGroupBox("Output")
        self.grpOutput.setLayout(self.outputLayout)
        self.grpInput = QtWidgets.QGroupBox("Input")
        self.grpInput.setLayout(self.inputLayout)
        self.grpBatch = QtWidgets.QGroupBox("Batch setup")
        self.grpBatch.setLayout(self.batchLayout)

        self.layout.addWidget(self.grpBatch)
        self.layout.addWidget(self.grpInput)
        self.layout.addWidget(self.grpOutput)
        self.layout.addWidget(self.grpProgress)
        self.layout.addWidget(self.btnStartCancel)

        # Initialize states
        self.btnStartCancel.setEnabled(False)
        self.chkAlignFrames.setChecked(True)


        self.edtStartFrame.setValue(0)
        self.edtEndFrame.setMaximum(399)
        self.edtEndFrame.setValue(199)


        self.edtSampleSize.setValue(3)
        self.output_path = "output"
        self.lineOutputDisplay.setText(self.output_path)
        self.setLayout(self.layout)

        self.edtNumCores.setMinimum(1)

        # Connect up signals and slots
        self.btnSelectData.clicked.connect(self.select_root)
        self.btnStartCancel.clicked.connect(self.startcancel)

        # Add in associated variables
        self.worker = Worker()
        self.files = {}
        self.progress_time_started = None

        self.resize(480, 300)
        self.setWindowTitle("Batch setup")
        self.worker.update.connect(self.updateprogress)

    def updateprogress(self, n, N, message):
        if N > 0:
            self.pbar_total.setMaximum(N)
        self.pbar_total.setValue(n)

    def selectoutput(self):
        self.output_path = QtWidgets.QFileDialog.getExistingDirectory(None, "Select output directory", self.output_path)
        self.lineOutputDisplay.setText(trunc(self.output_path))
        self.addLog("Output changed to : %s"%trunc(self.output_path))

    def select_root(self, parent=None):
        self.root_directory = str(QtWidgets.QFileDialog.getExistingDirectory(None, "Select root directory", ""))
        if not self.root_directory:
            return

        self.lblRootPath.setText(self.root_directory)

        paths = [ os.path.join(self.root_directory, p)
            for p in os.listdir(self.root_directory)]
        datapaths = filter( os.path.isdir, paths )
        for p in datapaths:
            files = utility.sort_filenames( glob.glob("%s/*tif"%p),
                time_regex = "TL(\d+)")
            if files:
                self.files[p] = files
        if not self.btnStartCancel.isEnabled():
            self.btnStartCancel.setEnabled(True)
            self.btnStartCancel.setText("Start")


    def startcancel(self):

        if not self.worker.isRunning():
            self.worker.num_cores = self.edtNumCores.value()
            self.worker.func = wrapped_main
            self.worker.arglists = []
            sample_factor = [self.edtSampleSize.value(), self.edtSampleSize.value()]
            start_frame = self.edtStartFrame.value()
            end_frame = self.edtEndFrame.value()

            for p in self.files:
                files = self.files[p]
                files = files[start_frame:(end_frame+1)]
                print("PATH : ", p)
                print(" -- %d FILES"%len(files))
                root = os.path.dirname(files[0])
                outpath = os.path.join(root, self.output_path)
                kwargs = dict(filenames = files,
                    output_path = outpath,
                    output_types = ['png', 'pdf'],
                    N_clusters = 10,
                    NLines = 1000,
                    align_frames=self.chkAlignFrames.isChecked(),
                    sample_factor=sample_factor,
                    )
                self.worker.arglists.append(
                    ((), kwargs))
            self.worker.start()
            self.btnStartCancel.setText("Cancel")
        else:
            # Kill all workers
            self.worker.terminate()
            self.btnStartCancel.setText("Start")
    def worker_finished(self, id):
        print("WORKER FINISHED", id)
        self.progressLayout.removeWidget(self.pbars[id])
        self.pbars[id].deleteLater()

    def worker_started(self, id):
        print("WORKER STARTED : ", id)
        self.pbars[id] = QtWidgets.QProgressBar(self)
        self.progressLayout.addWidget(self.pbars[id])


##=========================================================
## Test worker
##=========================================================

def test_main(self):
    import time
    import random
    import numpy as np
    t0 = time.time()
    ttot = random.randint(3,10)
    print("STARTING A WORKER FOR %d SECONDS"%ttot)
    while (time.time() - t0) < ttot:
        np.sqrt(np.random.rand(1000,1000,100))

##=========================================================
## Main
##=========================================================
if __name__ == '__main__':      # pragma: no cover

    app = QtWidgets.QApplication(sys.argv)
    app.setStyle("plastique")
    batcher = BatchDialog()
    batcher.show()
    app.exec_()
