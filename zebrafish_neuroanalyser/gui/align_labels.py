#!/usr/bin/env python

# embedding_in_qt4.py --- Simple Qt4 application embedding matplotlib canvases
#
# Copyright (C) 2005 Florent Rougon
#               2006 Darren Dale
#
# This file is an example program for matplotlib. It may be used and
# modified with no restriction; raw copies as well as modified versions
# may be distributed without limitation.

import argparse
import glob
import os
import string
import sys
import time

import numpy as np
import skimage.io as skio
from matplotlib.backends.backend_qt5agg \
        import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT
from matplotlib.figure import Figure

from PyQt5 import QtCore, QtGui, QtWidgets
import zebrafish_neuroanalyser.dataio as zio
from zebrafish_neuroanalyser.utility import logger

PWD = os.path.abspath(os.path.dirname(__file__))
ROOT = os.path.dirname(PWD)


class NavigationToolbar(NavigationToolbar2QT):
    #    # only display the buttons we need
    #    toolitems = [t for t in NavigationToolbar2QT.toolitems if
    #                 t[0] in ('Home', 'Pan', 'Zoom', 'Save')]
    #    print(toolitems)
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.main = self.parent.parent().parent().parent()
        actions = self.actions()
        for action in actions:
            text = action.text()
            if text not in ("Home", "Pan", "Zoom", "Save"):
                self.removeAction(action)

        self.action_pick = QtWidgets.QAction(
            QtGui.QIcon(os.path.join(PWD, "icon_crosshairs.png")),
            "&Pick points", self)
        self.action_pick.triggered.connect(self.press_pick)
        self.action_pick.setCheckable(True)

        self.action_save = QtWidgets.QAction(
            # QtGui.QIcon.fromTheme("document-save"),
            self.style().standardIcon(QtWidgets.QStyle.SP_DialogSaveButton
                                      # QtGui.QStyle.SP_DialogOpenButton
                                      ),
            "Save ROIs",
            self,
        )
        self.action_save.triggered.connect(self.press_save_rois)

        self.addAction(self.action_pick)
        self.addAction(self.action_save)

    def press_pick(self, *args):
        """Activate picking mode"""
        if self._active == "PICK":
            self._active = None
        else:
            for action in self.actions():
                if action is self.action_pick:
                    continue
                action.setChecked(False)
            self._active = "PICK"

        if self._idPress is not None:
            self._idPress = self.canvas.mpl_disconnect(self._idPress)
            self.mode = ""

        if self._idRelease is not None:
            self._idRelease = self.canvas.mpl_disconnect(self._idRelease)
            self.mode = ""

        if self._active:
            # self._idPress = self.canvas.mpl_connect('button_press_event',
            #                                        self.pick_point)
            self._idRelease = self.canvas.mpl_connect("button_release_event",
                                                      self.main.pick_point)
            self.mode = "pick point"
            self.canvas.widgetlock(self)
        else:
            self.canvas.widgetlock.release(self)

        for axis in self.canvas.figure.get_axes():
            axis.set_navigate_mode(self._active)

        self.set_message(self.mode)

    def press_save_rois(self):
        self.main.save_rois()


class DraggablePoint:
    lock = None

    def __init__(self, point, main, index, canvas):
        self.point = point
        self.main = main
        self.press = None
        self.bg = None
        self.index = index
        self.canvas = canvas
        self.axes = canvas.axes

    def connect(self):
        """
        Connect all needed events
        """
        self.cidpress = self.canvas.mpl_connect("button_press_event",
                                                self.on_press)
        self.cidrelease = self.canvas.mpl_connect("button_release_event",
                                                  self.on_release)
        self.cidmotion = self.canvas.mpl_connect("motion_notify_event",
                                                 self.on_motion)

    def on_press(self, event):
        "on button press we will see if the mouse is over us and store some data"
        if event.inaxes != self.axes:
            return
        if DraggablePoint.lock is not None:
            return
        contains, attrd = self.point.contains(event)
        if not contains:
            return
        xy = self.point.get_xydata()
        x0, y0 = xy[0]
        self.press = x0, y0, event.xdata, event.ydata
        DraggablePoint.lock = self

        # Set the current row
        self.main.table1.selectRow(self.index)

        # draw everything but the selected pointangle and store the pixel buffer
        self.point.set_animated(True)
        self.canvas.draw()
        self.bg = self.canvas.copy_from_bbox(self.axes.bbox)

        # now redraw just the point
        self.axes.draw_artist(self.point)

        # and blit just the redrawn area
        self.canvas.blit(self.axes.bbox)

    def on_motion(self, event):
        "on motion we will move the point if the mouse is over us"
        if DraggablePoint.lock is not self:
            return
        if event.inaxes != self.axes:
            return
        x0, y0, xpress, ypress = self.press
        dx = event.xdata - xpress
        dy = event.ydata - ypress
        self.point.set_data((x0 + dx, y0 + dy))

        z = self.main.zvalue
        i = self.main.table1.currentRow()
        self.main.set_point(i, x0 + dx, y0 + dy, z)

        self.main.update_points()

        # restore the bg region
        self.canvas.restore_region(self.bg)

        # redraw just the current pointangle
        self.axes.draw_artist(self.point)

        # blit just the redrawn area
        self.canvas.blit(self.axes.bbox)

    def on_release(self, event):
        "on release we reset the press data"
        if DraggablePoint.lock is not self:
            return

        self.press = None
        DraggablePoint.lock = None

        # turn off the point animation property and reset the bg
        self.point.set_animated(False)
        self.bg = None

        # redraw the full figure
        self.canvas.draw()

    def disconnect(self):
        "disconnect all the stored connection ids"
        self.canvas.mpl_disconnect(self.cidpress)
        self.canvas.mpl_disconnect(self.cidrelease)
        self.canvas.mpl_disconnect(self.cidmotion)


class MyMplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        FigureCanvas.__init__(self, fig)
        self.axes = fig.add_subplot(111)
        ## We want the axes cleared every time plot() is called
        # self.axes.hold(False)
        self.setParent(parent)
        self.main = self.parent().parent().parent()

        self.initiate_figure()

        FigureCanvas.setSizePolicy(self, QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

        self.toolbar = NavigationToolbar(self, self)

    def update_figure(self):
        pass


class MyStaticMplCanvas(MyMplCanvas):
    """Simple canvas with a sine plot."""
    def update_figure(self):
        self.im.set_data(self.main.data[self.main.zvalue])
        self.draw()

    def initiate_figure(self):
        self.axes.clear()
        self.im = self.axes.imshow(self.main.data[self.main.zvalue],
                                   cmap="gray")
        self.axes.set_autoscale_on(False)
        self.axes.xaxis.set_visible(False)
        self.axes.yaxis.set_visible(False)
        self.draw()


class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        QtWidgets.QApplication.setStyle(
            QtWidgets.QStyleFactory.create("Cleanlooks"))

        self.NPOINTS = 14
        self.ZMAX = 20
        self.zvalue = 0
        self.mode = "normal"
        self.title_normal = "Zfish point selector"
        self.title_reference = "WARNING: Editing Reference Points!!!"
        self.toggle_mode_text_normal = "&Switch to normal editing mode"
        self.toggle_mode_text_reference = "&Switch to generate new reference points"
        self.data_random = np.random.rand(self.ZMAX, 500, 500)
        self.data = self.data_random
        self.point_actors = []
        self.files = []

        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle(self.title_normal)

        self.file_menu = QtWidgets.QMenu("&File", self)
        self.file_menu.addAction("&Load Data", self.loadData,
                                 QtCore.Qt.CTRL + QtCore.Qt.Key_O)
        self.action_toggle_mode = self.file_menu.addAction(
            self.toggle_mode_text_reference,
            self.newReferencePoints,
            QtCore.Qt.CTRL + QtCore.Qt.Key_S,
        )
        self.file_menu.addAction("&Quit", self.fileQuit,
                                 QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)

        self.help_menu = QtWidgets.QMenu("&Help", self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)

        self.help_menu.addAction("&About", self.about)

        self.main_widget = QtWidgets.QWidget(self)

        self.layoutmain = QtWidgets.QVBoxLayout(self.main_widget)
        self.splitter1 = QtWidgets.QSplitter(QtCore.Qt.Horizontal, self)
        self.z_viewer = QtWidgets.QWidget(self.splitter1)
        self.layout_z = QtWidgets.QVBoxLayout(self.z_viewer)
        self.canvas = MyStaticMplCanvas(self.z_viewer,
                                        width=5,
                                        height=4,
                                        dpi=100)
        # dc1 = MyDynamicMplCanvas(self.main_widget, width=5, height=4, dpi=100)

        self.zslider = QtWidgets.QSlider(self)
        self.zslider.valueChanged.connect(self.slider_moved)
        self.layout_z.addWidget(self.zslider)
        self.table1 = QtWidgets.QTableWidget(self.NPOINTS, 3)

        self.splitter1.addWidget(self.canvas)
        self.splitter1.addWidget(self.table1)
        self.layoutmain.addWidget(self.splitter1)
        self.layout_z.addWidget(self.zslider)

        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)
        self.newDataLoaded()

        self.loadRef()
        self.populate_table()

        self.statusBar().showMessage("All hail matplotlib!", 2000)

    def populate_table(self):
        self.table1.setRowCount(self.NPOINTS)
        self.table1.setSelectionBehavior(
            QtWidgets.QAbstractItemView.SelectRows)
        self.table1.setCurrentCell(0, 0)
        self.table1.setHorizontalHeaderLabels(("x", "y", "z"))
        self.table1.setVerticalHeaderLabels(
            list(string.ascii_lowercase[i] for i in range(self.NPOINTS)))

        # self.zslider = QtWidgets.QSlider(self)
        # self.zslider.valueChanged.connect(self.slider_moved)

        # l.addWidget(dc1)

    def newReferencePoints(self):
        if self.mode == "normal":
            # First confirm this
            ans = QtWidgets.QMessageBox.question(
                self,
                "WARNING!!!",
                "\n".join([
                    "You will OVER-WRITE the previous reference points; ARE YOU SURE YOU WANT TO CONTINUE???",
                    "Take me back to safety, blue pill for me",
                    "I'm sure, I asked Matt, red pill please",
                ]),
            )
            if ans == 0:
                return
            # If user said yes, switch to reference point editing mode
            self.mode = "reference"
            self.statusBar().showMessage("Switching to reference editing mode",
                                         2000)
            self.fig_ref.hide()
            self.setWindowTitle(self.title_reference)
            self.action_toggle_mode.setText(self.toggle_mode_text_normal)
            self.loadRef3D()
            self.newDataLoaded()
            self.points = []
            self.table1.setRowCount(1)
        else:
            self.mode = "normal"
            self.statusBar().showMessage(
                "Switching to normal ROI editing mode", 2000)
            self.fig_ref.show()
            self.data = self.data_random
            self.newDataLoaded()
            self.setWindowTitle(self.title_normal)
            self.action_toggle_mode.setText(self.toggle_mode_text_reference)

    def loadRef(self):
        """
        Load the reference data (image and points)
        to a new figure
        """
        self.fig_ref = QtWidgets.QDialog(self)
        self.fig_ref.setWindowTitle("Reference")
        # self.fig_ref.setGeometry(0, 0, 212, 480)
        self.fig_ref.setGeometry(0, 0, 222, 490)
        view = QtWidgets.QGraphicsView(self.fig_ref)
        # view.setGeometry(0, 0, 212, 480)
        view.setGeometry(0, 0, 222, 490)
        scene = QtWidgets.QGraphicsScene(view)
        view.setScene(scene)
        # Use full ABSOLUTE path to the image, not relative
        fname = os.path.join(ROOT, "labels", "reference_brain_max.jpg")
        if not os.path.isfile(fname):
            logger.critical(
                "Reference brain image is missing [from: %s], cannnot proceed"
                % fname)
            raise Exception(
                "Reference brain image is missing, cannnot proceed")
        im = QtGui.QPixmap(fname)
        im.scaledToHeight(480)
        scalefact = 480.0 / 1406.0
        scene.addPixmap(im)
        view.show()
        # Now load the current labels and draw as circular points
        rois_file = os.path.join(ROOT, "labels", "rois.txt")
        pts = []
        letters = []
        with open(rois_file) as rfile:
            for line in rfile:
                parts = line.split(",")
                pts.append([float(s) for s in parts[1:]])
                letters.append(parts[0])
            # pts = np.array(pts)
            # pts = pts[:,[2,1,0]]

        # scene.setForegroundBrush(QtGui.QBrush(QtGui.QColor(255,0,0)))
        # QtGui.QPen(QtGui.QColor(1,0,0)),
        # QtGui.QBrush(),

        cols = ["blue", "green", "red", "cyan", "magenta", "yellow"]
        ncols = len(cols)
        npts = len(pts)
        circle_radius = 2
        font = QtGui.QFont()
        font.setBold(True)
        font.setPointSize(10)

        self.NPOINTS = len(pts)
        self.points = [[] for i in range(self.NPOINTS)]

        for i, (pt, lett) in enumerate(zip(pts, letters)):
            circle = QtWidgets.QGraphicsEllipseItem(
                scalefact * pt[1] - circle_radius,
                scalefact * pt[0] - circle_radius,
                2 * circle_radius,
                2 * circle_radius,
            )
            # colnow = QtGui.QColor(cols[i%len(cols)])
            colnow = QtGui.QColor.fromHsv(360 * (i / float(npts)), 255, 255)
            colfont = QtGui.QColor.fromHsv(360 * (i / float(npts)), 255, 255)
            pen = QtGui.QPen(colnow)
            pen.setWidth(2)
            circle.setPen(pen)
            scene.addItem(circle)
            text = scene.addText(lett, font)
            text.setDefaultTextColor(colfont)
            text.setPos(scalefact * pt[1] - 3, scalefact * pt[0] - 3)

        self.fig_ref.show()

    def loadRef3D(
            self,
            path=os.path.join(ROOT, "labels", "reference_brain_frames"),
            pattern="*jpg",
    ):
        # Load the reference stack data
        fname_pattern = os.path.join(path, pattern)
        fnames = sorted(glob.glob(fname_pattern))
        logger.debug("Loading 3d reference stack:")
        logger.debug("From          : %s" % path)
        logger.debug("Using pattern : %s" % pattern)
        logger.debug("[Glob reports %d files matching pattern]" %
                     len(glob.glob(fname_pattern)))
        self.data = skio.imread_collection(fname_pattern).concatenate()

    def slider_moved(self, value):
        self.zvalue = value
        self.canvas.update_figure()

    def newDataLoaded(self):
        self.ZMAX = self.data.shape[0]
        self.zslider.setMaximum(self.ZMAX - 1)
        self.canvas.initiate_figure()
        self.points = [[] for i in range(self.NPOINTS)]
        self.point_actors = []
        self.table1.clearContents()
        self.table1.selectRow(0)

    def loadData(self):
        # Pop up a select file dialog
        dlg = QtWidgets.QFileDialog(self)
        dlg.setWindowTitle("Select data")

        def ufunc(*args, **kwargs):
            print(kwargs, args)

        dlg.setFileMode(QtWidgets.QFileDialog.ExistingFiles)
        if dlg.exec_():
            # self.files = sorted([str(f) for f in dlg.selectedFiles()])
            files = [str(f) for f in dlg.selectedFiles()]
            print("FILES:")
            print(files)
            if len(files) == 1:
                # Assume z stack...
                self.files = files
                data = zio.load_and_resample(
                    files[0],
                    sample_factor=[1, 1],
                    update_function=ufunc,
                    align_on_load=True,
                    TMAX=5,
                )
                if data.ndim == 3:
                    self.data = data
                elif data.ndim == 4:
                    self.data = data.max(axis=0)
                else:
                    print("UNKNOWN FORMAT", data.shape)
                self.newDataLoaded()

    def pick_point(self, event):
        """the press mouse button in pick mode callback"""

        if event.button == 1:
            self._button_pressed = 1
        elif event.button == 3:
            self._button_pressed = 3
        else:
            self._button_pressed = None
            return

        x, y = event.xdata, event.ydata
        z = self.zvalue
        cr = self.table1.currentRow()
        self._xypress = []
        if self.mode == "reference":
            rowPosition = self.table1.rowCount()
            # If we're on the last point, add in new point
            if cr == (rowPosition - 1):
                self.table1.insertRow(rowPosition)
                self.table1.setVerticalHeaderItem(
                    rowPosition,
                    QtWidgets.QTableWidgetItem(
                        string.ascii_lowercase[rowPosition]),
                )
                self.points.append([])
        self.set_point(cr, x, y, z)
        self.table1.setCurrentCell(cr + 1, 0)

    def set_point(self, i, x, y, z):
        self.table1.setItem(i, 0, QtWidgets.QTableWidgetItem(str(x)))
        self.table1.setItem(i, 1, QtWidgets.QTableWidgetItem(str(y)))
        self.table1.setItem(i, 2, QtWidgets.QTableWidgetItem(str(z)))
        self.points[i] = (x, y, z)
        self.update_points()

    def save_rois(self):
        """
        Save the rois
        """
        if self.mode == "normal":
            # Make sure we're good to go
            if len(self.points) != self.NPOINTS:
                self.warning("There are not enough points!\nAborting save")
                return
            if not all(self.points):
                self.warning(
                    "Not all points have been set [%d points needed]!\nAborting save"
                    % self.NPOINTS)
                return
            print("SAVING ROIS")
            print(self.points)
            print(self.files)
            if not self.files:
                QtWidgets.QMessageBox.warning(
                    self,
                    "Warning: No data loaded",
                    "No data loaded, so cannot save rois",
                )
                return
            # Now save the ROI file
            savedir = "%s_ROIS" % self.files[0]
            if not os.path.isdir(savedir):
                os.makedirs(savedir)

            with open(os.path.join(savedir, "rois.txt"), "w") as fid:
                for lett, p in zip(string.ascii_lowercase, self.points):
                    fid.write("%s\n" %
                              ",".join([lett] + ["%0.2f" % v for v in p]))
        elif self.mode == "reference":
            npoints = len(self.points)
            ans = QtWidgets.QMessageBox.question(
                self,
                "FINAL WARNING!!!",
                "\n".join([
                    "You are about to OVER-WRITE the previous reference points using",
                    "%d new points" % npoints,
                    "ARE YOU SURE YOU WANT TO CONTINUE???",
                ]),
                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                QtWidgets.QMessageBox.No,
            )
            if ans == 0:
                return
            reference_rois_file = os.path.join(ROOT, "labels", "rois.txt")
            archive_rois_file = os.path.join(
                ROOT, "labels",
                "old_rois%s.txt" % time.strftime("%Y%m%d_%H%M"))
            if not os.path.isfile(reference_rois_file):
                QtWidgets.QMessageBox.warning(
                    self,
                    "Warning: No previous reference ROIs file found",
                    "No previous reference ROIs file found, could be a problem?",
                )
            else:
                # os.rename(reference_rois_file,
                print("Renaming ", reference_rois_file)
                print("to ", archive_rois_file)
                try:
                    os.rename(reference_rois_file, archive_rois_file)
                except:
                    print("FAILED TO RENAME :(")
                    print("THE OLD POINTS WILL BE GONE FOR GOOD!")

            with open(reference_rois_file, "w") as fid:
                for lett, p in zip(string.ascii_lowercase, self.points):
                    # fid.write("%s\n"%",".join([lett,] + ["%0.2f"%v for v in p]))
                    fid.write(
                        "%s\n" %
                        ",".join([lett] + ["%0.2f" % p[v] for v in [1, 0, 2]]))

    def update_points(self):
        """
        Updates the displayed points
        """
        for draggable in self.point_actors:
            draggable.point.remove()
        self.point_actors = []

        # Reset the colour cycle
        self.canvas.axes.set_prop_cycle(None)
        for i, point in enumerate(self.points):
            if not point:
                break
            draggable = DraggablePoint(
                self.canvas.axes.plot(
                    point[0],
                    point[1],
                    "o",
                    markeredgecolor="y",
                    markeredgewidth=3,
                    markersize=10,
                )[0],
                self,
                i,
                self.canvas,
            )
            draggable.connect()
            self.point_actors.append(draggable)

        self.canvas.draw()

    def fileQuit(self):
        """
        File->quit action, closes the window
        """
        self.close()

    def closeEvent(self, ce):
        """
        Close event handler
        """
        self.fileQuit()

    def warning(self, message):
        """
        Displays a warning message box
        """
        QtWidgets.QMessageBox.warning(self, "Warning", message)

    def about(self):
        """
        Displays an "about" dialog box
        """
        QtWidgets.QMessageBox.about(
            self, "About",
            """Simple Zfish label aligner\nCopyright 2016 Jeremy Metz""")


def run_align_gui():
    """
    Creates the QT application and runs the main window
    """
    qApp = QtWidgets.QApplication([])
    window = ApplicationWindow()
    window.show()
    sys.exit(qApp.exec_())


def main():
    """
    Main entry point
    """
    # parser = argparse.ArgumentParser()
    # parser.add_argument("filename", help="File to load", nargs="?")
    # parser.add_argument("--generate-reference",
    #                     help="Generate reference points",
    #                     action="store_true")
    # args = parser.parse_args()
    # kwargs = vars(args)
    run_align_gui()


if __name__ == "__main__":  # pragma: no cover
    raise Exception("This submodule should not be run as a script")
    main()
