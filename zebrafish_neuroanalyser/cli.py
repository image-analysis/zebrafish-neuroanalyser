#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : CLI
#
from zebrafish_neuroanalyser.utility import (
    logger,
    logging,
    load_config,
    default_error_function,
    default_update_function,
    default_get_filename_function,
    dummy_function,
    sort_filenames,
)


import zebrafish_neuroanalyser.utility as utility
import zebrafish_neuroanalyser.main as main
import argparse
import traceback
import sys
import os
import time

# To set the movie writer
import matplotlib.pyplot as plt
import matplotlib.animation as animation


def create_parser():
    parser = argparse.ArgumentParser(
        description='Brain image processing and analysis pipeline.')
    parser.add_argument('filenames', nargs="*")
    parser.add_argument('--web', action='store_true', default=False)
    parser.add_argument(
        '--aligngui', action='store_true', default=False,
        help="Show the GUI for aligning labels")
    parser.add_argument(
        '--reprocess', action='store_true', default=False,
        help="Reprocess the output files; generate fire heatmap")
    parser.add_argument('--nowriter', action='store_true', default=False)
    parser.add_argument(
        '--cli', action='store_true', default=False,
        help="Use Command Line Interface (no GUI)")
    parser.add_argument('--tmax', type=int, default=0)
    parser.add_argument('--debug', action='store_true', default=False)
    parser.add_argument('--config', help="Specify non-default config file")
    return parser


def run_cli(argv=None):
    """
    Parse the command line arguments and
    start the appropriate function
    """
    if argv is None:
        argv = sys.argv[1:]
    args = create_parser().parse_args(argv)
    if args.debug:
        # Set all handlers to debug
        for handler in logger.handlers:
            handler.setLevel(logging.DEBUG)
    config = load_config(filename=args.config)

    n_rois = None
    filenames = []
    output_path = None
    output_path_format = "{}_outputs"
    output_path_folder = "output"
    selected_rois_file = None

    if "matplotlib" in config:
        logger.info(
            "No longer using ffmpeg;"
            + " now using pillow writter to generate GIFs")
        # ffmpeg_path = config["matplotlib"].get("animation.ffmpeg_path")
        # if ffmpeg_path:
        #    plt.rcParams['animation.ffmpeg_path'] = ffmpeg_path
        #    logger.warning("\n".join([
        #        "setting pyplot.animation.ffmpeg_path to ",
        #        ffmpeg_path,
        #        "This only works if ffmpeg is runnable given this command!",
        #    ]))
    if "input/output" in config:
        section = config["input/output"]
        selected_rois_file = section.get("selected_rois_file")
        output_path_format = section.get("output_path_format", "{}_outputs")
        output_path_folder = section.get("output_path_folder", "output")
        logger.info("selected_rois_file: {}".format(selected_rois_file))
        logger.info("output_path_format: {}".format(output_path_format))
        logger.info("output_path_folder: {}".format(output_path_folder))

    # moviewriter = animation.FFMpegWriter
    if 'pillow' in animation.writers.avail:
        moviewriter = animation.writers['pillow']
    else:
        logger.warning("Pillow movie writer not available")
        moviewriter = None
    if args.nowriter:
        moviewriter = None
    if args.filenames:
        logger.debug("args.filenames:")
        logger.debug(str(args.filenames))
        logger.debug("filenames after sort:")
        filenames = sort_filenames(
            args.filenames,
            time_regex="TL(\\d+)")
        logger.debug(str(filenames))
        output_path = output_path_format.format(filenames[0])

    if not args.cli and args.aligngui:
        #
        # Try and launch the alignment GUI
        #
        logfile_dir = utility.LOGROOT
        logfile_filename = "align_gui_run_%s.log" % time.strftime(
            "%Y%m%d_%H%M%S")
        utility.add_file_handler_to_global_logger(
            output_path=logfile_dir, filename=logfile_filename)
        # sympath = os.path.join(logfile_dir, "align_gui_run_LAST.log")
        # if os.path.isfile(sympath):
        #     os.unlink(sympath)
        # os.symlink(os.path.join(logfile_dir, logfile_filename), sympath)
        logger.info("Starting align_labels GUI...")
        try:
            import zebrafish_neuroanalyser.gui.align_labels as align_labels
            align_labels.run_align_gui()
        except Exception:
            logger.critical("align_labels failed:")
            logger.critical(traceback.format_exc())
            raise
    elif not args.cli and args.reprocess:
        #
        # Try and launch the reprocessing
        #
        logfile_dir = utility.LOGROOT
        logfile_filename = "reprocess_outputs_run_%s.log" % time.strftime(
            "%Y%m%d_%H%M%S")
        utility.add_file_handler_to_global_logger(
            output_path=logfile_dir, filename=logfile_filename)
        logger.info("Starting reprocessing of outputs...")
        try:
            import zebrafish_neuroanalyser.reprocess as reprocess
            reprocess.run()
        except Exception:
            logger.critical("Reprocessing failed:")
            logger.critical(traceback.format_exc())
            raise
    elif not args.cli:
        try:
            if args.web:
                import zebrafish_neuroanalyser.gui.gui_web as gui
            else:
                from zebrafish_neuroanalyser.gui import qt5 as gui
            gui.main_runner(
                main.main,
                filenames,
                moviewriter=moviewriter,
                output_path_folder=output_path_folder,
                selected_rois_file=selected_rois_file,
            )

        except Exception:
            logger.critical("Failed to use Qt")
            logger.critical(traceback.format_exc())
            logger.critical(
                "No graphical libraries available, using console fallback")
            logger.info("Starting processing pipeline...")
            main.main(
                filenames=filenames,
                update_function=default_update_function,
                output_path=output_path,
                error_message_function=default_error_function,
                get_filename_function=default_get_filename_function,
                moviewriter=moviewriter,
                selected_rois_file=selected_rois_file,
            )
    else:

        logger.info("Selected CLI")
        logger.info("Starting processing pipeline...")
        try:
            main.main(
                filenames=filenames,
                update_function=default_update_function,
                output_path=output_path,
                error_message_function=lambda m: logger.error(
                    "Error! : %s" % m),
                get_filename_function=default_get_filename_function,
                TMAX=args.tmax,
                n_rois=n_rois,
                moviewriter=moviewriter,
                selected_rois_file=selected_rois_file,
            )
        except Exception:
            logger.critical("Main pipeline (main.main) failed:")
            logger.critical(traceback.format_exc())
            raise
