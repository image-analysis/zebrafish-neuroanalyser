from __future__ import print_function

import multiprocessing
from zebrafish_neuroanalyser import main, utility
import os
import tempfile
import progressbar
import glob

def wrapped_main(argskwargs):
    main.main(*argskwargs[0], **argskwargs[1])

def run_batch(
        testrun = False,
        root_directory = None,
        cores = 1,
        **runkwargs
        ):
    print("Creating Process pool...")
    pool = multiprocessing.Pool(cores)
    func = wrapped_main

    if testrun:
        arglists = generate_test_arglists()
    else:
        arglists = generate_arglists(root_directory,
            **runkwargs)

    N = len(arglists)
    print("Running %d jobs..."%N)

    pbar = progressbar.ProgressBar(
        maxval = N,
        widgets = [ "Batch processing:",
            progressbar.Percentage(), ' ',
            progressbar.Bar(), ' ',
            progressbar.ETA()],
    )
    pbar.start()
    for i, _ in enumerate(pool.imap_unordered(func, arglists) ):
        pbar.update(i)

def generate_arglists(root, **runkwargs):
    if root is None:
        raise Exception('Root directory not supplied')

    arglists = []
    #------------------------------
    # Get all subfolders
    #------------------------------
    paths = [ os.path.join(root, p)
        for p in os.listdir(root)]
    datapaths = filter( os.path.isdir, paths )
    allfiles = {}
    for p in datapaths:
        files = glob.glob("%s/*tif"%p)
        files = utility.sort_filenames( files, time_regex = "TL(\d+)")
        if files:
            allfiles[p] = files
    N = len(allfiles)
    print("Found %d data directories, creating arguments"%N)
    for path in allfiles:
        rootnow = os.path.dirname(allfiles[path][0])
        outpath = os.path.join(rootnow,
            runkwargs.get("output_path", "output"))
        kwargs = {}
        kwargs.update(runkwargs,
            filenames = allfiles[path],
            output_path = outpath)
        arglists.append(((), kwargs))

    return arglists

def generate_test_arglists(**runkwargs):
    N = 12
    arglists = []
    outpath = os.path.join( tempfile.gettempdir(), os.path.basename(__file__))
    sample_factor = [2,2]
    utility.logger.info("Creating %d test runs"%N)
    for i in range(N):
        kwargs = {}
        kwargs.update(runkwargs,  filenames = ["dev",],
            output_path = outpath+str(i),
            align_frames=False,
            sample_factor=sample_factor,
            write_film=False,
            )
        arglists.append(((), kwargs))
    return arglists

##=========================================================
## Main
##=========================================================

if __name__ == "__main__":      # pragma: no cover

    # Get the root directory
    import PyQt4.QtGui as QtGui
    app = QtGui.QApplication([])
    rootpath = QtGui.QFileDialog.getExistingDirectory(
        None, "Select root data directory", "")
    if not rootpath:
        print("Batch operation cancelled (no root folder selected")
        sys.exit()
    else:
        rootpath = str(rootpath)
    # Run the batch
    run_batch( root_directory = rootpath,
        cores = 4)
