"""dataio.py

Data IO related functions

J. Metz <metz.jp@gmail.com>
"""
import csv
import glob
import io
import json
import os
import time
import traceback
import warnings  # To suppress OLE-TIFF loading warnings...
import zipfile

import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np
import scipy.ndimage as ndi
import skimage.io as skio
import skimage.measure as skmeas
from skimage.external import tifffile

from zebrafish_neuroanalyser.registration import align_frame
from zebrafish_neuroanalyser.utility import default_update_function, logger


def load_and_resample(
        filenames,
        sample_factor=[3, 3],
        update_function=None,
        align_on_load=True,
        TMIN=0,
        TMAX=0,
        return_shape0=False,
        save_resampled=True,
        load_resampled=True,
):
    """
    TODO: ENter more description here

    Alignnment notes:
    Use a standard registration method to align the stack.
    NOTE: In Thunder they use cross-correlation (calculated using numpy.fftn)
          between mean of 20 frames (reference) and current frame

    Approach
    --------

    We will use either of the
    * running mean (keep updating the mean image)
    * whole data set mean

    The running mean approach (similar to Thunder) by design demarks the
    first frame as special, in that the second frame is moved to align with the
    first, and so on.
    The whole mean approach determins the absolute average frame. This has the
    benefit that if, for example, the image shifts after a 3rd of the film,
    the reference frame will look more like the last 2/3rds of the film
    than the first half, thus moving the first frames in stead of the last
    2/3rd, which maximized the amount of useable pixels.

    This may however fail if the image is "shakey" - in which case a more
    "frame-to-frame" approach will most likely be best.
    """
    if update_function is None:
        update_function = default_update_function

    update_function(message="Loading and resampling data ")
    if load_resampled:
        data, data0_shape = load_resampled_data(
            filenames=filenames,
            sample_factor=sample_factor,
            align_on_load=align_on_load,
            TMIN=TMIN,
            TMAX=TMAX,
        )
        if data is not None:
            update_function(message="Loading previously resampled data")
            if return_shape0:
                return data, data0_shape
            else:
                return data
        else:
            update_function(
                message="Unable to load previously resampled data, re-loading")

    logger.info('Align frames is %s' % ('ON' if align_on_load else 'OFF'))
    if isinstance(filenames, str):
        filenames = [filenames, ]
    if len(filenames) == 1:
        update_function(message='Loading data...')
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            data = skioimread(filenames[0], plugin="tifffile")
        if TMAX == 0:
            TMAX = len(data)
        N = TMAX - TMIN
        data = data[TMIN:TMAX]
        shape0 = data.shape
        if data.ndim not in (3, 4):
            raise Exception('Can currently only handle 3 or 4 d data')
        if align_on_load:
            # Iterate over time index and do mean-frame based alignment
            # NOTE: data already the substack here
            mean = data[0]
            update_function(0, N, message='Aligning...')
            for n, frame in enumerate(data):
                new_frame = align_frame(mean, frame)
                data[n] = new_frame
                # mean += (new_frame / (n+2)).astype(mean.dtype)
                # From http://math.stackexchange.com/a/106720
                mean = (mean.astype(float)
                        + (new_frame.astype(float)-mean.astype(float)) / (n+1)
                        ).astype(mean.dtype)
                update_function(n+1, N)
            update_function(message='Finished aligning')

        data = skmeas.block_reduce(
            data,
            block_size=tuple([1 for n in range(data.ndim-2)]+sample_factor),
            func=np.mean)
    else:
        # Sort the filenames, and load and resample each one
        if TMAX == 0:
            TMAX = len(filenames)
        N = TMAX - TMIN
        if align_on_load:
            update_function(
                0, N,
                message='Loading and aligning %d frames...[%d --> %d]' %
                (N, TMIN, TMAX))
        else:
            update_function(0, N, message='Loading (without aligning)...')
        # data0 = skio.imread(
        #    filenames[TMIN], plugin="tifffile", multifile=False)
        data0 = tifffile.imread(filenames[TMIN], multifile=False)
        shape0 = (N, ) + data0.shape
        update_function(0, N, message="Loaded frame 0: " + str(data0.shape))
        if align_on_load:
            mean = data0.copy()
        data0 = skmeas.block_reduce(
            data0,
            block_size=tuple([1 for n in range(data0.ndim-2)]+sample_factor),
            func=np.mean)
        data = np.empty((N,) + data0.shape)
        if data.ndim not in (3, 4):
            raise Exception('Can currently only handle 3 or 4 d data')
        data[0] = data0
        for i, f in enumerate(filenames[(TMIN+1):TMAX]):
            frame = tifffile.imread(f, multifile=False)
            if align_on_load:
                # DEBUG
                # plt.close('all')
                frame = align_frame(mean, frame)
                # mean  += (frame / (i+2)).astype(mean.dtype)
                # From http://math.stackexchange.com/a/106720
                mean = (mean.astype(float)
                        + (frame.astype(float)-mean.astype(float)) / (i+1)
                        ).astype(mean.dtype)
            data[i+1] = skmeas.block_reduce(
                frame,
                block_size=tuple(
                    [1 for n in range(data0.ndim-2)]+sample_factor),
                func=np.mean,
            )
            update_function(n=i+2, N=N)
        update_function(message='Finished loading')
    update_function(message='Final loaded shape: %s' % str(data.shape))

    if save_resampled:
        save_resampled_data(
            data,
            data0_shape=shape0,
            filenames=filenames,
            sample_factor=sample_factor,
            align_on_load=align_on_load,
            TMIN=TMIN,
            TMAX=TMAX,
        )

    if return_shape0:
        return data, shape0
    return data


def save_resampled_data(
        data,
        filenames=[],
        data0_shape=(),
        **kwargs
):
    """
    Save the resampled data in a well defined location
    """
    outfolder = "%s_intermediate" % filenames[0]
    metadata_file = os.path.join(outfolder, "load_parameters.json")
    data_file = os.path.join(outfolder, "resampled_data.tif")
    if not os.path.isdir(outfolder):
        os.makedirs(outfolder)
    # Save the metadata
    kwargs["filenames"] = filenames
    kwargs["data0_shape"] = data0_shape
    with open(metadata_file, "w") as fout:
        json.dump(kwargs, fout)
    # Now save the resmapled data
    skio.imsave(data_file, data)


def load_resampled_data(
        filenames=[],
        **kwargs
):
    """
    Load the resampled data if it exists
    """
    outfolder = "%s_intermediate" % filenames[0]
    metadata_file = os.path.join(outfolder, "load_parameters.json")
    data_file = os.path.join(outfolder, "resampled_data.tif")
    if not os.path.isdir(outfolder):
        logger.debug(
            "Intermediate folder does not exist, not loading resampled data")
        return None, None
    if not os.path.isfile(metadata_file):
        logger.debug(
            "Metadata file does not exist, not loading resampled data")
        return None, None
    if not os.path.isfile(data_file):
        logger.debug(
            "Resampled data file does not exist, not loading resampled data")
        return None, None
    # Load the metadata
    kwargs["filenames"] = filenames
    with open(metadata_file) as fin:
        metadata = json.load(fin)
        data0_shape = metadata.get("data0_shape", ())
        if not data0_shape:
            logger.debug(
                "Metadata data0_shape not set, not loading resampled data")
            return None, None

        # Now make sure the metadata is the same
        if metadata != kwargs:
            logger.debug("Metadata not the same, not loading resampled data")
            return None, None
    # Now save the resmapled data
    return skio.imread(data_file), data0_shape


def skioimread(*args, **kwargs):
    """
    Avoids OLE-TIFF errors when all files listed in the meta data dont exist
    """
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        return skio.imread(*args, **kwargs)


def skioimsave(*args, **kwargs):
    """
    Avoids annoying "low-contrast image" warnings...
    """
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        return skio.imsave(*args, **kwargs)


def put_in_archive(files, archive, flat=True):
    if not isinstance(files, (list, tuple)):
        files = [files, ]
    for fname in files:
        # Ignore folder structure (i.e. don't want this in archive)
        dest = os.path.basename(fname) if flat else fname
        try:
            with zipfile.ZipFile(
                    archive, "a",
                    compression=zipfile.ZIP_DEFLATED) as output_archive:
                output_archive.write(fname, arcname=dest)
            os.remove(fname)
        except Exception:
            logger.warning(
                "Unable to place %s into output archive because:" % fname)
            logger.warning(traceback.format_exc())


def load_points_from_file(filename, shuffle=None):
    with open(filename) as rfile:
        pts = []
        for line in rfile:
            pts.append([float(s) for s in line.split(",")[1:]])
        pts = np.array(pts)
        if shuffle is not None:
            pts = pts[:, shuffle]
    return pts


def load_aligned_rois_from_file(
        filenames=[],
        **kwargs
):
    outfolder = "%s_intermediate" % filenames[0]
    metadata_file = os.path.join(outfolder, "roi_parameters.json")
    roinames_file = os.path.join(outfolder, "roi_names.json")
    roi_file = os.path.join(outfolder, "aligned_rois.zip")
    if not os.path.isfile(metadata_file):
        return None, None
    if not os.path.isfile(roinames_file):
        return None, None
    if not os.path.isfile(roi_file):
        return None, None
    with open(metadata_file) as fin:
        metadata = json.load(fin)
        kwargs["filenames"] = filenames
        for k, v in kwargs.items():
            if isinstance(v, np.ndarray):
                kwargs[k] = v.tolist()
        if kwargs != metadata:
            logger.debug("Kwargs and loaded metadata mismatch")
            return None, None
    # Good to go, load the rois and roinames!
    with open(roinames_file) as fin:
        roinames0 = json.load(fin)
    roinames = {int(k): v for k, v in roinames0.items()}
    roiarchive = zipfile.ZipFile(roi_file)
    roifilebase = kwargs["roifilebase"]
    allrois = {}
    for num in kwargs["selected"]:
        name = roifilebase % num
        try:
            allrois[num] = skio.imread(
                io.BytesIO(roiarchive.read(name)), plugin='tifffile') > 0
        except Exception:
            return None, None
    return allrois, roinames


def get_outfolder_and_filenames_for_saving_rois(filenames):
    outfolder = "%s_intermediate" % filenames[0]
    metadata_file = os.path.join(outfolder, "roi_parameters.json")
    roinames_file = os.path.join(outfolder, "roi_names.json")
    roi_file = os.path.join(outfolder, "aligned_rois.zip")
    if not os.path.isdir(outfolder):
        os.makedirs(outfolder)
    return outfolder, metadata_file, roinames_file, roi_file


def write_metadata_to_file(filename, metadata):
    with open(filename, "w") as fout:
        for k, v in metadata.items():
            if isinstance(v, np.ndarray):
                metadata[k] = v.tolist()
        json.dump(metadata, fout)


def write_roinames_to_file(filename, roinames):
    with open(filename, "w") as fout:
        json.dump(roinames, fout)


def write_rois_to_zip_archive(filename, roifilebase, selected, allrois):
    roiarchive = zipfile.ZipFile(
        filename, "w", compression=zipfile.ZIP_DEFLATED)
    for num in selected:
        name = roifilebase % num
        fileobj = io.BytesIO()
        tifffile.imsave(fileobj, allrois[num].astype('uint8'))
        fileobj.seek(0)
        roiarchive.writestr(name, fileobj.read())


def save_aligned_rois_to_file(
        allrois,
        roinames,
        filenames=[],
        **kwargs
):
    """
    Take in all the rois and their names, as well as the filenames, and
    save to disk.
    Save the rois themselves into a zipfile as uint8 tiffs.
    """
    (outfolder,
     metadata_file,
     roinames_file,
     roifile) = get_outfolder_and_filenames_for_saving_rois(filenames)
    kwargs["filenames"] = filenames
    write_metadata_to_file(metadata_file, kwargs)
    write_roinames_to_file(roinames_file, roinames)
    roifilebase = kwargs["roifilebase"]
    selected = kwargs["selected"]
    write_rois_to_zip_archive(roifile, roifilebase, selected, allrois)


def get_rois(
        selection_file,
        roifilearchive,  # roifiledir,
        roifilebase,
        affine,
        shift,
        data0_shape,
        sample_factor,
        n_rois,
        update_function=default_update_function,
        save_aligned_rois=True,
        load_aligned_rois=True,
        filenames=[],
):
    # Load selected regions
    with open(selection_file) as fid:
        # Should be at least length 3
        selected0 = [l.split() for l in fid if l.strip()]
        selected = [int(l[0]) for l in selected0]
        roinames = {int(l[0]): l[2] for l in selected0}
    selected = selected[:n_rois]
    Nroi = len(selected)
    if load_aligned_rois:
        allrois_loaded, roinames_loaded = load_aligned_rois_from_file(
            filenames=filenames,
            selected=selected,
            affine=affine,
            shift=shift,
            n_rois=n_rois,
            roifilebase=roifilebase,
        )
        if allrois_loaded is not None:
            update_function(message="Loaded previously aligned regions")
            return allrois_loaded, roinames_loaded

    # When using a zipped archive...
    roiarchive = zipfile.ZipFile(roifilearchive)
    update_function(message="Extracting, aligning, and resampling regions...")
    allrois = {}
    t0 = time.time()
    for i, roiname in enumerate(selected):
        # load the roi label
        roifile = roifilebase % roiname
        lbl0 = skioimread(
            io.BytesIO(roiarchive.read(roifile)), plugin='tifffile')
        # lbl0 = skioimread( os.path.join(roifiledir, roifile))

        lbl = lbl0.transpose([0, 2, 1])

        # Transform the roi label using the
        # given transformation
        lbl2_0 = ndi.affine_transform(
            lbl,
            affine,
            offset=shift,
            output_shape=data0_shape[1:],
            # order=1,
        )
        # Now need to resample in same way as data
        # NOTE: use median so we don't get weird effects

        lbl2 = skmeas.block_reduce(
            lbl2_0,
            block_size=tuple([1 for n in range(lbl2_0.ndim-2)]+sample_factor),
            func=np.median)
        lbl2 = lbl2 > 0

        # To be able to overlay all the rois
        allrois[roiname] = lbl2
        update_function(n=i, N=Nroi)
    roiarchive.close()
    dt = time.time()-t0
    update_function(
        message="Extraction done in %f s (%fs / region)" % (dt, dt/Nroi))
    if save_aligned_rois:
        save_aligned_rois_to_file(
            allrois,
            roinames,
            filenames=filenames,
            selected=selected,
            affine=affine,
            shift=shift,
            n_rois=n_rois,
            roifilebase=roifilebase,
        )
    return allrois, roinames


def write_film_file(
        data, output_path, writer,
        update_function=default_update_function):
    """
    Use selected writer to create an animation of the data
    """
    update_function(message="Creating maximum intensity projection film")
    size = data.shape[-2:]
    fig = plt.figure(
        figsize=(10*float(size[1])/size[0], 10)
    )
    plt.axes([0, 0, 1, 1])
    im = plt.imshow(data[0].max(axis=0), cmap='jet', origin='upper')
    num_frames = data.shape[0]

    def anim_max_data(ind):
        frame = data[ind].max(axis=0)
        im.set_data(frame)
        update_function(n=ind, N=num_frames)
    try:
        anim = animation.FuncAnimation(fig, anim_max_data,
                                       frames=num_frames)
        writer = writer(fps=15, bitrate=1800)
        anim.save(os.path.join(output_path, 'maximum_projection_film.gif'),
                  writer=writer)
    except Exception:
        update_function(message="Unable to write movie, skipping...")
        logger.debug("Movie writing failed with error:")
        logger.debug(traceback.format_exc())
    plt.close(fig)


def save_full_profiles_to_csv(
        lines,
        linelabels,
        output_path,
        output_archive_filename,
        method="line-by-line"):
    # Save cluster index + full profile data
    column_headers = ["", "Cluster ID", ] + ["t = %d" % t
                                             for t in range(len(lines[0]))]
    row_labels = np.array(["Pixel %d" % i
                           for i in range(len(lines))])[:, np.newaxis]
    filename_full_profiles = os.path.join(output_path, "full_profiles.csv")
    # This duplicates the array - awful for memory!
    if method == "numpy":
        np.savetxt(
            filename_full_profiles,
            np.vstack((
                column_headers,
                np.hstack((
                    row_labels,
                    linelabels[:, np.newaxis],
                    lines,
                )),
            )),
            delimiter=",",
            fmt="%s",
        )
    elif method == "line-by-line":
        # Use basic csv writing instead
        with open(filename_full_profiles, "w") as fout:
            wrt = csv.writer(fout)
            wrt.writerow(column_headers)
            for rowlabel, linelabel, linenow in zip(
                    row_labels, linelabels, lines):
                wrt.writerow([rowlabel,  linelabel] + linenow.tolist())
    else:
        raise Exception("Invalid writing method specified")
    put_in_archive(filename_full_profiles, output_archive_filename)
    logger.info("OUTPUT: %s  -  %s" % (
        "full_profiles",
        "Full profile data with cluster identity"
        + " (for every pixel) - excel importable")),


def save_cluster_means_to_csv(
        cluster_means, cluster_stdv, N_clusters, output_path, name=""):
    if name:
        name = "%s_" % name
    np.savetxt(
        os.path.join(output_path, "%scluster_means_plus_stdvs.csv" % name),
        np.vstack((
            ["Means", ] + ["" for t in range(len(cluster_means[0]))],
            ["Cluster ID", ] + ["t = %d" %
                                t for t in range(len(cluster_means[0]))],
            np.hstack((np.arange(N_clusters)[:, np.newaxis], cluster_means)),
            ["Standard deviation", ] +
            ["" for t in range(len(cluster_means[0]))],
            ["Cluster ID", ] + ["t = %d" %
                                t for t in range(len(cluster_stdv[0]))],
            np.hstack((np.arange(N_clusters)[:, np.newaxis], cluster_stdv)),
        )),
        delimiter=",",
        fmt="%s",
    )


def save_corrected_cluster_means_to_csv(corrected_means, output_path):
    with open(os.path.join(output_path,
                           "corrected_cluster_means.csv"), 'w') as csvout:
        wrt = csv.writer(csvout)
        for method, mns in corrected_means.items():
            shp = mns.shape
            wrt.writerows([
                ["Method : %s" % method, ],
                ["Cluster ID", ] + ["t = %d" % t for t in range(shp[1])],
            ])
            wrt.writerows([
                [i, ] + mns[i].tolist() for i in range(shp[0])
            ])
    logger.info("OUTPUT: %s  -  %s" % (
        "cluster_means_plus_stdvs",
        "Cluster means & standard deviations - excel importable"))


def save_labelled_region_means_to_csv(stats, roinames, output_path, name=""):
    if name:
        name = "%s_" % name
    with open(os.path.join(
            output_path,
            "%slabelled_region_means_plus_stdvs.csv" % name), "w") as fout:
        wrt = csv.writer(fout)
        wrt.writerow(["Means"])
        wrt.writerow(["Region ID", ]+[
            "t = %d" % t
            for t in range(len(list(stats.values())[0]['mean']))])
        wrt.writerows([
            ["%d - %s" % (rid, roinames[rid])] + stat['mean']
            for rid, stat in sorted(stats.items())
        ])
        wrt.writerow(["Standard deviation"])
        wrt.writerow(["Region ID", ]+[
            "t = %d" % t
            for t in range(len(list(stats.values())[0]['stdv']))])
        wrt.writerows([
            ["%d - %s" % (rid, roinames[rid])] + stat['stdv']
            for rid, stat in stats.items()
        ])


def save_corrected_labelled_region_means_to_csv(
        stats,
        corrected_means,
        roinames,
        output_path,
        name=""):
    if name:
        name = "_%s" % name
    with open(os.path.join(
            output_path,
            "corrected_labelled_region_stats%s.csv" % name), 'w') as csvout:
        wrt = csv.writer(csvout)
        for method in corrected_means:
            wrt.writerow(["Medians (corrected using %s)" % method])
            wrt.writerow(["Region ID", ]+[
                "t = %d" % t
                for t in range(len(list(stats.values())[0]['mean']))])
            wrt.writerows([
                ["%d - %s" % (rid, roinames[rid])] +
                list(stat['corrected medians'][method])
                for rid, stat in sorted(stats.items())
            ])
        for method in corrected_means:
            wrt.writerow(["Means (corrected using %s)" % method])
            wrt.writerow(["Region ID", ]+[
                "t = %d" % t
                for t in range(len(list(stats.values())[0]['mean']))])
            wrt.writerows([
                ["%d - %s" % (rid, roinames[rid])] +
                list(stat['corrected means'][method])
                for rid, stat in sorted(stats.items())
            ])
        for method in corrected_means:
            wrt.writerow(["Standard deviation (corrected using %s)" % method])
            wrt.writerow(["Region ID", ]+[
                "t = %d" % t
                for t in range(len(list(stats.values())[0]['mean']))])
            wrt.writerows([
                ["%d - %s" % (rid, roinames[rid])] +
                list(stat['corrected stdv'][method])
                for rid, stat in sorted(stats.items())
            ])


def get_all_region_trace_files(output_folder, recursive=False):
    """
    Scans `output_folder` for region trace files, i.e. files that match:
    corrected_labelled_region_stats_normalised.csv
    or corrected_labelled_region_stats_unnormalised.csv
    """
    if recursive:
        pattern = "**/corrected_labelled_region_stats_*normalised.csv"
    else:
        pattern = "corrected_labelled_region_stats_*normalised.csv"
    allfiles = glob.glob(pattern, recursive=recursive)
    return allfiles


def load_stats_from_tracefile(tracefile):
    """
    Load stats from (CSV) tracefile, back into the stats structure
    Stats structure is:
        {
            <REGION ID 1> : {
                                'mean' : [...],
                                'corrected medians': [...],
                                'corrected means': [...],
                                'corrected stdv': [...],
                                ...
                            },
            <REGION ID 2> : ...
        }
    of which only the `corrected medians`, `corrected means`, and
    `corrected stdv` are present in these files.
    """
    csv_sections = (
        ('Medians (corrected using ', 'corrected medians'),
        ('Means (corrected using ', 'corrected means'),
        ('Standard deviation (corrected using ', 'corrected stdv'),
    )

    # Get the "name" component from the filename
    # Format is corrected_labelled_region_stats[_NAME].csv
    filename = os.path.basename(tracefile)
    correctionname = os.path.splitext(filename)[0].split(
        "corrected_labelled_region")[1]
    if correctionname:
        # If name is given, probably starts with "_"
        correctionname = correctionname.lstrip('_')

    stats = {}
    with open(tracefile) as csvin:
        rdr = csv.reader(csvin, dialect='excel')
        row = next(rdr)
        while not row:
            row = next(rdr)
        for isection, (sectiontext, sectionname) in enumerate(csv_sections):
            if isection < (len(csv_sections)-1):
                nextsectiontext = csv_sections[isection+1][0]
            if not row[0].startswith(sectiontext):
                message = (
                    "Section does not start with {}\n  "
                    + "Instead it is: [{}]").format(sectiontext, row[0])
                raise ValueError("Bad csv file [{}]\n Detail: {}".format(
                    tracefile, message))
            method = row[0].lstrip(sectiontext).rstrip(')')
            header = next(rdr)
            if not header:
                header = next(rdr)
            if header[0] != "Region ID":
                message = (
                    "Header row firt cell is not 'Region ID'\n  "
                    + "Instead it is: [{}]").format(header[0])
                raise ValueError("Bad csv file [{}]\n Detail: {}".format(
                    tracefile, message))
            # Now read rows until next section
            for row in rdr:
                # Annoyingly, we seem to have blank lines...
                if not row:
                    continue
                # If we start a new section, break this loop
                # Otherwise it continues to the end of file
                if row[0].startswith(nextsectiontext):
                    break
                regionidname, *values = row
                values = np.array([float(value) for value in values])
                regionid = int(regionidname.split(' - ')[0])
                stats.setdefault(regionid, {})
                stats[regionid].setdefault(sectionname, {})
                stats[regionid][sectionname][method] = values
        return stats, correctionname
